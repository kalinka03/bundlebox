<?php include("template/header.php") ?>
<?php include("template/header-main.php") ?>
<link rel="stylesheet" href="css/news/news.css">
<link rel="stylesheet" href="css/news/media.css">


<div class="descnews ">
    <div class="container">
        <div class="descnews__bread ">
            <a href="index.php">
                Главная
            </a>
            <a href="news.php">
                Новости
            </a>
            <span class="descnews__news-name">
            Заголовок новости, который отображает ...
        </span>
        </div>
        <div class="row descnews__title-bg">
            <div class="col-lg-5  col-md-7 col-12">
                <div class="  descnews__title">
                    <div class="descnews__date">
                <span>
                    24.07.2018
                </span>
                    </div>
                    <div class="descnews__name">
                <span>
                    СОБЫТИЯ
                </span>
                    </div>
                    <div class="descnews__arrow">
                <span>
                    Акция до 5.05
                </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="descnews__img">
            <img src="img/news/descnews__img.png" alt="descnews__img">
        </div>
        <div class="newsinfo__wrapper row justify-content-center">
            <div class="descnews__text newsinfo col-sm-10 col-12 ">
                <div class="newsinfo__title">
                    <h2>
                        Заголовок новости, который отображает возможную длину новости или акции размером более одной
                    </h2>
                </div>
                <div class="newsinfo__content">
                    <h2>
                        Heading 3
                    </h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                        Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                        sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                        mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus
                        pronin sapien nunc accuan eget.

                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                        Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                        sic tempor.
                        Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus
                        pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.
                        Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet
                        lacus accumsan et viverra
                        justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis
                        parturient montes, nascetur
                        ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                        rhoncus pronin sapien
                        nunc accuan eget.
                    </p>
                    <h3>
                        Heading 4
                    </h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                        Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                        sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                        mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus
                        pronin sapien nunc accuan eget.

                    </p>
                    <h3>
                        Heading 4
                    </h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                        Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                        sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                        mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus
                        pronin sapien nunc accuan eget.

                    </p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.
                        Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                        sic tempor.
                        Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus
                        pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.
                        Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet
                        lacus accumsan et viverra
                        justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis
                        parturient montes, nascetur
                        ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed
                        rhoncus pronin sapien
                        nunc accuan eget.
                    </p>
                    <h4>
                        Heading 5
                    </h4>
                    <ul>
                        <li>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </li>
                        <li>
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra
                            justo commodo.
                        </li>
                        <li>
                            Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus.
                        </li>
                        <li>
                            Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin
                            sapien nunc accuan eget.
                        </li>
                    </ul>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin
                        gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic
                        tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam
                        fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien
                        nunc accuan eget.

                    </p>
                    <ol>
                        <li>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </li>
                        <li>
                            Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra
                            justo commodo.
                        </li>
                        <li>
                            Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus.
                        </li>
                        <li>
                            Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin
                            sapien nunc accuan eget.
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="share">
            <div class="share__title">
                <h2>
                    Поделитесь с друзьями:
                </h2>
            </div>
            <div class="pluso" data-background="transparent"
                 data-options="medium,round,line,horizontal,nocounter,theme=06"
                 data-services="twitter,facebook,linkedin,pinterest,google"></div>
        </div>
    </div>
    <div class="helpful container">
        <div class="helpful__title">
            <h2>
                Полезная информация:
            </h2>
        </div>
        <div class="row helpful__blocks helpful__blocks-js">
            <div class="col-md-4">
                <div class="helpful__block-wrapper " data-aos="zoom-in" data-aos-once="true">
                    <a href="#">
                        <div class="helpful__block">
                            <img class="helpfull__img" src="img/news/helpful1.png" alt="helpful1">
                            <div class="helpfull__arrow">
                                <img src="img/news/Arrow.svg" alt="arrow">
                            </div>
                        </div>
                    </a>
                    <div class="helpful__block-text">
                        <p>
                            Заголовок новости, который отображает возможную длину новости или акции размером более одной строки
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="helpful__block-wrapper " data-aos="zoom-in">
                    <a href="#">
                        <div class="helpful__block">
                            <img class="helpfull__img" src="img/news/helpful2.png" alt="helpful1">
                            <div class="helpfull__arrow">
                                <img src="img/news/Arrow.svg" alt="arrow">
                            </div>
                        </div>
                    </a>
                    <div class="helpful__block-text">
                        <p>
                            Заголовок новости, который
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="helpful__block-wrapper " data-aos="zoom-in">
                    <a href="#">
                        <div class="helpful__block">
                            <img class="helpfull__img" src="img/news/helpful3.png" alt="helpful1">
                            <div class="helpfull__arrow">
                                <img src="img/news/Arrow.svg" alt="arrow">
                            </div>
                        </div>
                    </a>
                    <div class="helpful__block-text">
                        <p>
                            Заголовок новости, который отображает возможную длину новости
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<?php include("template/footer.php") ?>
<?php include("template/popupMain.php") ?>
<script type="text/javascript" src="/js/main/sliderSale.js"></script>
<script type="text/javascript" src="/js/main/validateMain.js"></script>
<script type="text/javascript" src="/js/fancyboxMain.js"></script>
<script src="js/news/news.js"></script>

</body>
</html>
