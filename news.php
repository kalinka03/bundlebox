<?php include("template/header.php") ?>
<?php include("template/header-main.php") ?>
<link rel="stylesheet" href="plugin/hover__master/hover.css">
<link rel="stylesheet" href="css/news/news.css">
<link rel="stylesheet" href="css/news/media.css">

<main class="news__wrap">
    <div class="news__top">
        <div class=" news__wrapper container">
            <div class="news__content ">
                <div class="news__title ">
                    <h2>Новости</h2>
                    <div class="news__img">
                        <img src="img/news/title.png" alt="title__news">
                    </div>
                </div>
                <div class="news__tabs">
                    <div class="news__tabs-title">
                        <h2>Категория:</h2>
                    </div>
                    <ul class="tabs__caption">
                        <li class="active" data-tab="0">
                            <a  href="#">ВСЕ</a>
                        </li>
                        <li data-tab="1" class="news__active" >
                            <a href="#">АКЦИИ</a>

                        </li>
                        <li data-tab="2" >
                            <a href="#">СОБЫТИЯ</a>
                        </li>
                        <li data-tab="3">
                            <a href="#">МАГАЗИНЫ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="tabs__content">
        <div class="news__blocks container active__blocks">
            <div data-aos="fade-up" class="news__block" >
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news1.png" alt="news1">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>-50%</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            АКЦИИ
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-up" class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news2.png" alt="news2">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>Акция до 5.05</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            События
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-up" class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news3.png" alt="news3">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>Акция до 5.05</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            Магазины
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-up" class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news4.png" alt="news4">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>-30%</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            Акция
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-up"  class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news5.png" alt="news5">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>Акция до 26.10</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            События
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="news__blocks container ">
            <div data-aos="fade-up" class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news4.png" alt="news4">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>-30%</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            Акция
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-up"  class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news5.png" alt="news5">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>Акция до 26.10</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            События
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-up" class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news2.png" alt="news2">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>Акция до 5.05</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            События
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-up" class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news3.png" alt="news3">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>Акция до 5.05</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            Магазины
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="news__blocks container ">
            <div data-aos="fade-up" class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news2.png" alt="news2">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>Акция до 5.05</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            События
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-aos="fade-up" class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news3.png" alt="news3">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>Акция до 5.05</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            Магазины
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="news__blocks container ">
            <div data-aos="fade-up" class="news__block">
                <div class="row justify-content-between">
                    <div class="col-lg-6 col-12">
                        <div class="news__pict">
                            <a href="descnews.php">
                                <img src="img/news/news3.png" alt="news3">
                            </a>
                            <div class="news__pict-arrow wow fadeInLeft" data-wow-offset="200"
                                 data-wow-delay="0.5s"
                                 data-wow-duration="1s" >
                                <span>Акция до 5.05</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 news__info">
                        <div class="news__info-title">
                            <h2>
                                Заголовок акции, который отображает возможную длину новости или акции размером более одной строки
                            </h2>
                        </div>
                        <div class="news__info-text">
                            <p>
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях . . .
                            </p>
                        </div>
                        <div class="row justify-content-between news__info-foot">
                            <div class=" col-sm-6 col-6 info__date">
                        <span class="info__date-write">
                            24.07.2018
                        </span>
                                <span class="info__shops">
                            Магазины
                        </span>
                            </div>
                            <div class="col-sm-6 col-6 info__detailed">
                                <a href="descnews.php">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="news__pagination">
        <section class="section-pagination">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-pagination__content">
                            <a class="arrow-left " href="#">
                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15.6 15.5" style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                        <style type="text/css">
                            .st0 {
                                fill: #434343;
                            }
                        </style>
                                    <g>
                                        <g>
                                            <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                        			c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                        			C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"></path>
                                        </g>
                                    </g>

                        </svg>
                            </a>
                            <a class="active number" href="#">1</a>
                            <a class="number" href="#">2</a>
                            <a class="number" href="#">3</a>
                            <a class="more" href="#">...</a>
                            <a class="number" href="#">12</a>
                            <a class="number" href="#">13</a>
                            <a class="arrow-right" href="#">
                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 15.6 15.5" style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                        <style type="text/css">
                            .st0 {
                                fill: #434343;
                            }
                        </style>
                                    <g>
                                        <g>
                                            <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                        			c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                        			C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"></path>
                                        </g>
                                    </g>

                        </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


</main>

<?php include("template/footer.php") ?>
<?php include("template/popupMain.php") ?>
<script type="text/javascript" src="/js/main/sliderSale.js"></script>
<script type="text/javascript" src="/js/main/validateMain.js"></script>
<script type="text/javascript" src="/js/fancyboxMain.js"></script>
<script src="js/news/news.js"></script>

</body>
</html>
