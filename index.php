<?php include("template/header-main.php") ?>
<?php //include("template/header-lk.php") ?>

<section class="wrapper js-paralax-window">
    <div class="parallax-viewport">
        <ul id="parallax">
            <li data-depth="0.1">
                <img class="parallax-layer1 parallax-layer" src="img/main/block2/sircle.png" alt="">
            </li>
            <li data-depth="0.2">
                <img class="parallax-layer3 parallax-layer" src="img/main/block2/sneakers_right.png" alt="">
            </li>
            <li data-depth="0.2">
                <img class="parallax-layer4 parallax-layer" src="img/main/block2/sneakers_left.png" alt="">
            </li>
            <li data-depth="0.4">
                <img class="parallax-layer9 parallax-layer" src="img/main/block2/white.png" alt="">
            </li>
            <li data-depth="0.4">
                <img class="parallax-layer5 parallax-layer" src="img/main/block2/laptop.png" alt="">
            </li>
            <li data-depth="0.3">
                <img class="parallax-layer2 parallax-layer" src="img/main/block2/portfolio.png" alt="">
            </li>
            <li data-depth="0.4">
                <img class="parallax-layer6 parallax-layer" src="img/main/block2/flower.png" alt="">
            </li>
            <li data-depth="0.5">
                <img class="parallax-layer7 parallax-layer" src="img/main/block2/dress.png" alt="">
            </li>
            <li data-depth="0.6">
                <img class="parallax-layer8 parallax-layer" src="img/main/block2/duck.png" alt="">
            </li>
        </ul>
    </div>

    <div class="shopping">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="shopping__section">
                        <div class="shopping__content">
                            <div class="shopping__wrapper">
                                <h1>Шопинг по всему миру </h1>
                                <h3>с очень дешевой доставкой</h3>
                            </div>
                            <div class="shopping__foto">
                                <img src="img/main/block2/all-foto.png" alt="">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
<section class="simple-shopping-steps">
    <div class="container">
        <div class="simple-shopping__content-all">
            <div class="row">
                <div class="col-md-12">
                    <div class="simple-shopping__wrapper">
                        <div class="simple-shopping-steps__title">
                            <h2>3 Простых шага покупки <span>в магазинах США и Европы</span></h2>
                        </div>
                        <div class="simple-shopping-steps__third wow slideInDown" data-wow-offset="200"
                             data-wow-delay="0.5s"
                             data-wow-duration="2s">
                            <p>3 шага</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="planeta">
                        <?php include("template/planeta.php") ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="simple-shopping-steps__section">
                        <div class="z_1">
                            <div class="simple-shopping-steps__sircle">

                            </div>
                            <div class="simple-shopping-steps__section__item">
                                <div class="simple-shopping-steps__section__item__number">
                                    <h3>01</h3>
                                </div>
                                <div class="simple-shopping-steps__section__item__title">
                                    <p>Сделать заказ</p>
                                </div>
                            </div>
                        </div>
                        <div class="simple-shopping-steps__information z_1_2">
                            <p>Мы предлагаем зарегистрированным пользователям несколько способов оформления заказа – все
                                для
                                оптимизации процесса покупки и сокращения расходов на доставку</p>
                        </div>
                    </div>
                    <div class="simple-shopping-steps__section">
                        <div class="z_2">
                            <div class="simple-shopping-steps__sircle__two">

                            </div>
                            <div class="simple-shopping-steps__section__item">
                                <div class="simple-shopping-steps__section__item__number">
                                    <h3>02</h3>
                                </div>
                                <div class="simple-shopping-steps__section__item__title">
                                    <p>Оплатить и ждать посылку</p>
                                </div>
                            </div>
                        </div>
                        <div class="simple-shopping-steps__information z_2_1">
                            <p>Оплатить покупку Вы можете самостоятельно при оформлении заказа на сайте магазина или в
                                личном кабинете на нашем сайте.</p>
                            <p> Остается только ждать получения посылки. Все остальное мы сделаем сами.</p>
                        </div>
                    </div>
                    <div class="simple-shopping-steps__section">
                        <div class="z_3">
                            <div class="simple-shopping-steps__sircle__third">

                            </div>
                            <div class="simple-shopping-steps__section__item">
                                <div class="simple-shopping-steps__section__item__number">
                                    <h3>03</h3>
                                </div>
                                <div class="simple-shopping-steps__section__item__title">
                                    <p>Получить посылку в своем городе</p>
                                </div>
                            </div>
                        </div>
                        <div class="simple-shopping-steps__information z_3_1">
                            <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между покупкой
                                в
                                рунете и за рубежом становится незаметной.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="advantage-purchases">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="advantage-purchases__man-foto">
                    <div class="advantage-purchases__shadow wow fadeInLeftBig" data-wow-offset="200"
                         data-wow-delay="0.5s"
                         data-wow-duration="2s">
                        <img src="img/main/block4/shadow_elem.png" alt="">
                    </div>
                    <div class="advantage-purchases__foto wow fadeInLeftBig" data-wow-offset="300"
                         data-wow-delay="0.7s"
                         data-wow-duration="2s">
                        <img src="img/main/block4/foto_man.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="advantage-purchases__title">
                    <h2>Преимущества покупок</h2>
                    <p>с BUNDLE BOX </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="purchases-item">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xm-12">
                <div class="purchases-item__list" data-aos="fade-down"
                     data-aos-easing="linear"
                     data-aos-duration="500" data-aos-delay="100" data-aos-once="true">
                    <div class="purchases-item__icon">
                        <img src="img/main/block3/1.svg" alt="">
                    </div>
                    <div class="purchases-item__text">
                        <p>Мы очень хорошо знаем свою сферу деятельности и поэтому готовы выполнить любой запрос клиента
                            или найдем достойную альтернативу</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 ">
                <div class="purchases-item__list" data-aos="fade-down"
                     data-aos-easing="linear"
                     data-aos-duration="1500" data-aos-delay="300" data-aos-once="true">
                    <div class="purchases-item__icon">
                        <img src="img/main/block3/3.svg" alt="">
                    </div>
                    <div class="purchases-item__text">
                        <p>Простота заказа – можно выбрать любой удобный способ заказать товар и его доставку</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="purchases-item__list" data-aos="fade-down"
                     data-aos-easing="linear"
                     data-aos-duration=2000" data-aos-delay="500" data-aos-once="true">
                    <div class="purchases-item__icon">
                        <img src="img/main/block3/2.svg" alt="">
                    </div>
                    <div class="purchases-item__text">
                        <p>Мы помогаем экономить: групповые покупки, найти компанию для получения скидки, актуальные
                            акции – все в одном месте</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-4 col-xs-12" data-aos="fade-down"
                 data-aos-easing="linear"
                 data-aos-duration="500" data-aos-delay="100" data-aos-once="true">
                <div class="purchases-item__list">
                    <div class="purchases-item__icon">
                        <img src="img/main/block3/4.svg" alt="">
                    </div>
                    <div class="purchases-item__text">
                        <p>У нас прозрачные тарифы и самая низкая стоимость доставки</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12" data-aos="fade-down"
                 data-aos-easing="linear"
                 data-aos-duration="1500" data-aos-delay="100" data-aos-once="true">
                <div class="purchases-item__list">
                    <div class="purchases-item__icon">
                        <img src="img/main/block3/5.svg" alt="">
                    </div>
                    <div class="purchases-item__text">
                        <p>Доставим заказ в любую точку России</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="shop-wrap">
    <div class="shopping-shops">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="backg_yellow">
                        <div class="shopping-shops__title">
                            <h2>ТОП-10 магазинов <span> для шопинга</span></h2>
                        </div>
                        <div class="simple-shopping-steps__top-ten">
                            <p>ТОП 10</p>
                        </div>
                        <div class="shopping-shops__tabs">
                            <ul class="shopping-shops__tabs__title">
                                <li class="active"><a target="_blank" href="#">ЖЕНСКАЯ ОДЕЖДА</a></li>
                                <li><a target="_blank" href="#">МУЖСКАЯ ОДЕЖДА</a></li>
                                <li><a target="_blank" href="#">ДЕТСКАЯ ОДЕЖДА</a></li>
                                <li><a target="_blank" href="#">ИГРУШКИ</a></li>
                                <li><a target="_blank" href="#">ТЕХНИКА</a></li>
                                <li><a target="_blank" href="#">Для дома</a></li>
                                <li class="last-other"><a target="_blank" href="#">Другое</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="shopping-shops__tabs__content">
                    <div class="shopping-shops__tabs__choice active">
                        <ul class="gride slick__gride">
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/1.png" alt="">
                                </a>
                            </li>
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/2.png" alt="">
                                </a>
                            </li>
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/3.png" alt="">
                                </a>
                            </li>
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/4.png" alt="">
                                </a>
                            </li>
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/5.png" alt="">
                                </a>
                            </li>
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/6.png" alt="">
                                </a>
                            </li>
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/7.png" alt="">
                                </a>
                            </li>
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/8.png" alt="">
                                </a>
                            </li>
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/9.png" alt="">
                                </a>
                            </li>
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/10.png" alt="">
                                </a>
                            </li>
                        </ul>
                        <div class="slider-masonry-arrow">
                            <div class="prev_arrow">
                            </div>
                            <div class="next_arrow">
                            </div>
                        </div>
                        <div class="shopping-shops__tabs__btn">
                            <a href="#" class="button__orange tabs__orange">Смотреть каталог магазинов</a>
                        </div>
                    </div>
                    <div class="shopping-shops__tabs__choice">
                        <ul class="gride">
                            <li class="shown">
                                <a href="#js_shop" class="modal-trigger">
                                    <img class="element-item" src="/img/main/block5/1.png" alt="">
                                </a>
                            </li>
                        </ul>
                        <div class="shopping-shops__tabs__btn">
                            <a href="#" class="button__orange tabs__orange">Смотреть каталог магазинов</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="profitable-offers">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="profitable-offers__wrapper">
                    <div class="profitable-offers__left" data-aos="fade-right" data-aos-easing="linear"
                         data-aos-duration="500" data-aos-delay="300" data-aos-once="true">
                        <img src="img/main/block5-1/left-foto.jpg" alt="">
                    </div>
                    <div class="profitable-offers__center">
                        <h3>Не пропустите </h3>
                        <h4>выгодные предложения</h4>
                        <div class="arrow">
                            <img src="img/main/block5-1/Arrow_long.svg" alt="">
                        </div>
                    </div>

                    <div class="profitable-offers__right" data-aos="fade-left" data-aos-easing="linear"
                         data-aos-duration="500" data-aos-delay="300" data-aos-once="true">
                        <img src="img/main/block5-1/right-foto.png" alt="">
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!--<section class="slider-akcii">-->
<!--    <div class="owl-carousel owl-theme slider-akcii__item">-->
<!--        <div class="item">-->
<!--            <div class="container">-->
<!--                <div class="row">-->
<!--                    <div class="col-lg-6 col-sm-12 slider_ak" data-animation-in="fadeInLeft"-->
<!--                         data-animation-out="fadeInRight">-->
<!--                        <div class="persent">-->
<!--                            <p>-50%</p>-->
<!--                        </div>-->
<!--                        <img src="img/main/block5-1/slider/foto1.png" class="img-responsive slide-message"-->
<!--                             data-animation-in="fadeInLeft" data-animation-out="fadeInLeft">-->
<!--                    </div>-->
<!--                    <div class="col-lg-6 col-sm-12  slider-akcii__content__all hidden-xs"-->
<!--                         data-animation-in="fadeInRight"-->
<!--                         data-animation-out="fadeInLeft">-->
<!--                        <div>-->
<!--                            <h4>Заголовок акции, который отображает возможную длину новости или акции размером-->
<!--                                более-->
<!--                                одной строки</h4>-->
<!--                            <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между-->
<!--                                покупкой в рунете и за рубежом становится незаметной.</p>-->
<!--                            <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>-->
<!--                            <div class="slider-akcii__content__timer">-->
<!--                                <div class="DateTimer" id="timer1" datetime="August 30, 2018 13:51:50"></div>-->
<!--                            </div>-->
<!--                            <div class="go_shop">-->
<!--                                <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="item">-->
<!--            <div class="container">-->
<!--                <div class="row">-->
<!--                    <div class="col-lg-6 col-sm-12 slider_ak" data-animation-in="fadeInLeft"-->
<!--                         data-animation-out="fadeInRight">-->
<!--                        <div class="persent">-->
<!--                            <p>-50%</p>-->
<!--                        </div>-->
<!--                        <img src="img/main/block6/foto2.png" class="img-responsive slide-message"-->
<!--                             data-animation-in="fadeInLeft" data-animation-out="fadeInLeft">-->
<!--                    </div>-->
<!--                    <div class="col-lg-6 col-sm-12  slider-akcii__content__all hidden-xs"-->
<!--                         data-animation-in="fadeInRight"-->
<!--                         data-animation-out="fadeInLeft">-->
<!--                        <div>-->
<!--                            <h4>Заголовок акции, который отображает возможную длину новости или акции размером-->
<!--                                более-->
<!--                                одной строки</h4>-->
<!--                            <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между-->
<!--                                покупкой в рунете и за рубежом становится незаметной.</p>-->
<!--                            <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>-->
<!--                            <div class="slider-akcii__content__timer">-->
<!--                                <div class="DateTimer" id="timer10" datetime="August 28, 2018 13:51:50"></div>-->
<!--                            </div>-->
<!--                            <div class="go_shop">-->
<!--                                <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="item">-->
<!--            <div class="container">-->
<!--                <div class="row">-->
<!--                    <div class="col-lg-6 col-sm-12 slider_ak" data-animation-in="fadeInLeft"-->
<!--                         data-animation-out="fadeInRight">-->
<!--                        <div class="persent">-->
<!--                            <p>-50%</p>-->
<!--                        </div>-->
<!--                        <img src="img/main/block6/foto1.png" class="img-responsive slide-message"-->
<!--                             data-animation-in="fadeInLeft" data-animation-out="fadeInLeft">-->
<!--                    </div>-->
<!--                    <div class="col-lg-6 col-sm-12  slider-akcii__content__all hidden-xs"-->
<!--                         data-animation-in="fadeInRight"-->
<!--                         data-animation-out="fadeInLeft">-->
<!--                        <div>-->
<!--                            <h4>Заголовок акции, который отображает возможную длину новости или акции размером-->
<!--                                более-->
<!--                                одной строки</h4>-->
<!--                            <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между-->
<!--                                покупкой в рунете и за рубежом становится незаметной.</p>-->
<!--                            <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>-->
<!--                            <div class="slider-akcii__content__timer">-->
<!--                                <div class="DateTimer" id="timer11" datetime="August 29, 2018 13:51:50"></div>-->
<!--                            </div>-->
<!--                            <div class="go_shop">-->
<!--                                <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="item">-->
<!--            <div class="container">-->
<!--                <div class="row">-->
<!--                    <div class="col-lg-6 col-sm-12 slider_ak" data-animation-in="fadeInLeft"-->
<!--                         data-animation-out="fadeInRight">-->
<!--                        <div class="persent">-->
<!--                            <p>-50%</p>-->
<!--                        </div>-->
<!--                        <img src="img/main/block5-1/slider/foto1.png" class="img-responsive slide-message"-->
<!--                             data-animation-in="fadeInLeft" data-animation-out="fadeInLeft">-->
<!--                    </div>-->
<!--                    <div class="col-lg-6 col-sm-12  slider-akcii__content__all hidden-xs"-->
<!--                         data-animation-in="fadeInRight"-->
<!--                         data-animation-out="fadeInLeft">-->
<!--                        <div>-->
<!--                            <h4>Заголовок акции, который отображает возможную длину новости или акции размером-->
<!--                                более-->
<!--                                одной строки</h4>-->
<!--                            <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между-->
<!--                                покупкой в рунете и за рубежом становится незаметной.</p>-->
<!--                            <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>-->
<!--                            <div class="slider-akcii__content__timer">-->
<!--                                <div class="DateTimer" id="timer13" datetime="August 30, 2018 13:51:50"></div>-->
<!--                            </div>-->
<!--                            <div class="go_shop">-->
<!--                                <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--</section>-->


</section>
<section class="last-order">
    <div class="close_order"></div>
    <h4>ПОСЛЕДНИЙ ОФОРМЛЕННЫЙ ЗАКАЗ</h4>
    <div class="last-order__someOrder">
        <div class="last-order__someOrder__name">
            <a href="#">Вязаные свитера толстовка Fred Perry</a>
        </div>
        <div class="last-order__someOrder__price">
            <p>6425 RUB <span> / </span> 100 USD</p>
        </div>
    </div>
    <div class="last-order__someOrder">
        <div class="last-order__someOrder__name">
            <a href="#">Вязаные свитера толстовка Fred Perrydgdfg dfg dfg fdg</a>
        </div>
        <div class="last-order__someOrder__price">
            <p>6425 RUB <span> / </span> 100 USD</p>
        </div>
    </div>
</section>
<section class="slider-akcii">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-12 slider__picture_sale">
                <div  class="owl-carousel owl-theme loop-test">
                    <div class="item">
                        <div class="persent">
                            <p>-50%</p>
                        </div>
                        <img src="img/main/block5-1/slider/foto1.png">
                    </div>
                    <div class="item">
                        <div class="persent">
                            <p>-50%</p>
                        </div>
                        <img src="img/main/block6/foto2.png">
                    </div>
                    <div class="item">
                        <div class="persent">
                            <p>-50%</p>
                        </div>
                        <img src="img/main/block6/foto3.png">
                    </div>

                </div>

            </div>
            <div class="col-lg-6 col-sm-12 slider__sale__akcii">
                <div  class="owl-carousel owl-theme loop-test2">
                    <div class="item">
                        <div class="slider-akcii__content__all">
                            <div>
                                <h4>Заголовок акции, который отображает возможную длину новости или акции размером
                                    более
                                    одной строки</h4>
                                <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между
                                    покупкой в рунете и за рубежом становится незаметной.</p>
                                <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>
                                <div class="slider-akcii__content__timer">
                                    <div class="DateTimer" id="timer13" datetime="September  30, 2018 13:51:50"></div>
                                </div>
                                <div class="go_shop">
                                    <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="slider-akcii__content__all">
                            <div>
                                <h4>Заголовок акции, который отображает возможную длину новости или акции размером
                                    более
                                    одной строки</h4>
                                <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между
                                    покупкой в рунете и за рубежом становится незаметной.</p>
                                <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>
                                <div class="slider-akcii__content__timer">
                                    <div class="DateTimer" id="timer14" datetime="September  30, 2018 13:51:50"></div>
                                </div>
                                <div class="go_shop">
                                    <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="slider-akcii__content__all">
                            <div>
                                <h4>Заголовок акции, который отображает возможную длину новости или акции размером
                                    более
                                    одной строки</h4>
                                <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между
                                    покупкой в рунете и за рубежом становится незаметной.</p>
                                <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>
                                <div class="slider-akcii__content__timer">
                                    <div class="DateTimer" id="timer15" datetime="September 30, 2018 13:51:50"></div>
                                </div>
                                <div class="go_shop">
                                    <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>







<section class="together-cheaper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="together-cheaper__title">
                    <h2>Вместе – дешевле!</h2>
                    <h4>Соберите компанию для приобретения желаемого товара или участия в акции</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col_together">
                    <div class="together-cheaper__item">
                        <div class="together-cheaper__item__nameShop" data-aos="fade-right" data-aos-easing="linear"
                             data-aos-duration="500" data-aos-delay="100" data-aos-once="true">
                            <p>DESIGNER SHOE WAREHOUSE</p>
                        </div>
                        <div class="together-cheaper__item__foto">
                            <img src="img/main/block6/foto1.png" alt="">
                        </div>
                        <h5>Заголовок акции, который отображает возможное описание, заданное при создании группового
                            заказа, может быть в четыре строки. Группового</h5>
                        <div class="together-cheaper__item__amount-discount">
                            <div class="together-cheaper__item__amount">
                                <p>Размер скидки: </p>
                            </div>
                            <div class="together-cheaper__item__discount">
                                <p>до 25% по программе лояльности</p>
                            </div>
                        </div>
                        <div class="together-cheaper__item__counter">
                            <p>ДО ЗАВЕРШЕНИЯ ОСТАЛОСЬ</p>
                            <div class="timer__blog">
                                <div class="DateTimer_small" id="timer4" datetime="September 27, 2018 13:51:50"></div>
                            </div>
                            <div class="together-cheaper__item__counter__information">
                                <div class="l-col">
                                    <p>Компания: <span>10 чел.</span></p>
                                </div>
                                <div class="r-col">
                                    <a class="send_to_tabs modal-trigger" href="#js_enterSite">В закладки</a>
                                    <div class="close__tab"></div>
                                </div>
                            </div>
                        </div>
                        <div class="together-cheaper__item__come-shop">
                            <a href="#">Перейти в магазин</a>
                        </div>
                        <div class="together-cheaper__item__join">
                            <a href="#" class="button__dark">Присоединиться</a>
                        </div>

                    </div>
                    <div class="together-cheaper__item">
                        <div class="together-cheaper__item__nameShop" data-aos="fade-right" data-aos-easing="linear"
                             data-aos-duration="500" data-aos-delay="300" data-aos-once="true">
                            <p>Название магазина</p>
                        </div>
                        <div class="together-cheaper__item__foto">
                            <img src="img/main/block6/foto2.png" alt="">
                        </div>
                        <h5>Заголовок акции, который отображает возможное описание, заданное при создании группового
                            заказа, может быть в четыре строки. Группового</h5>
                        <div class="together-cheaper__item__amount-discount">
                            <div class="together-cheaper__item__amount">
                                <p>Размер скидки: </p>
                            </div>
                            <div class="together-cheaper__item__discount">
                                <p>до -80% дополнительно</p>
                            </div>
                        </div>
                        <div class="together-cheaper__item__counter">
                            <p>ДО КОНЦА СУММЫ ОСТАЛОСЬ</p>

                            <div class="together-cheaper__item_price">
                                <p>246 <span> $ </span></p>
                            </div>
                            <div class="together-cheaper__item__counter__information">
                                <div class="l-col">
                                    <p>Компания: <span>10 чел.</span></p>
                                </div>
                                <div class="r-col">
                                    <a class="send_to_tabs modal-trigger" href="#js_enterSite">В закладки</a>
                                    <div class="close__tab"></div>
                                </div>
                            </div>
                        </div>
                        <div class="together-cheaper__item__come-shop">
                            <a href="#">Перейти в магазин</a>
                        </div>
                        <div class="together-cheaper__item__join">
                            <a href="#" class="button__dark">Присоединиться</a>
                        </div>

                    </div>
                    <div class="together-cheaper__item">
                        <div class="together-cheaper__item__nameShop" data-aos="fade-right" data-aos-easing="linear"
                             data-aos-duration="500" data-aos-delay="500" data-aos-once="true">
                            <p>DESIGNER SHOE WAREHOUSE</p>
                        </div>
                        <div class="together-cheaper__item__foto">
                            <img src="img/main/block6/foto3.png" alt="">


                        </div>
                        <h5>Заголовок акции, который отображает возможное описание, заданное при создании группового
                            заказа.</h5>
                        <div class="together-cheaper__item__amount-discount">
                            <div class="together-cheaper__item__amount">
                                <p>Размер скидки: </p>
                            </div>
                            <div class="together-cheaper__item__discount">
                                <p>значение введеное при создании группового заказа.</p>
                            </div>
                        </div>
                        <div class="together-cheaper__item__counter">
                            <p>ДЛЯ КОМПАНИИ ОСТАЛОСЬ НАЙТИ</p>
                            <div class="together-cheaper__item_price">
                                <p>1 <span> чел. </span></p>
                            </div>
                            <div class="together-cheaper__item__counter__information">
                                <div class="l-col">
                                    <p>Компания: <span>10 чел.</span></p>
                                </div>
                                <div class="r-col">
                                    <a class="send_to_tabs modal-trigger" href="#js_enterSite">В закладки</a>
                                    <div class="close__tab"></div>
                                </div>
                            </div>
                        </div>
                        <div class="together-cheaper__item__come-shop">
                            <a href="#">Перейти в магазин</a>
                        </div>
                        <div class="together-cheaper__item__join">
                            <a href="#" class="button__dark">Присоединиться</a>
                        </div>

                    </div>
                </div>
                <div class="show__cheaper">
                    <a href="#" class="button__orange tabs__orange">Смотреть все предложения</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="question">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="question__wrapper">
                    <div class="question__foto-left">
                        <div class="question__foto-left__sircle wow fadeInLeftBig" data-wow-offset="100"
                             data-wow-delay="0.4s"
                             data-wow-duration="2s">
                            <img src="img/main/block7/backg.png" alt="">
                        </div>
                        <div class="question__foto-left__qust wow fadeInLeftBig" data-wow-offset="100"
                             data-wow-delay="0.6s"
                             data-wow-duration="3s">
                            <img src="img/main/block7/qust.png" alt="">
                        </div>
                    </div>
                    <div class="question__content__right">
                        <p>5 вопросов</p>
                    </div>
                    <div class="question__content">
                        <h2>
                            5 вопросов,
                        </h2>
                        <h3>которые возникают перед первой покупкой </h3>
                        <div class="question__content__all-questions">
                            <div class="tabs_questions">
                                <div class="tabs_questions__line active">
                                    <div class="tabs_questions__title">
                                        <span class="quest__plus"><i class="fas fa-plus"></i></span>
                                        <h5>Как оформить заказ?</h5>
                                    </div>
                                    <div class="tabs_questions__information" style="display: none;">
                                        <p>
                                            Повседневная практика показывает, что новая модель организационной
                                            деятельности позволяет выполнять важные задания по разработке системы
                                            обучения кадров, соответствует насущным потребностям. С другой стороны
                                            начало повседневной работы по формированию позиции способствует
                                            подготовки и
                                            реализации направлений прогрессивного развития. Значимость этих проблем
                                            настолько очевидна, что дальнейшее развитие различных форм деятельности
                                            способствует подготовки и реализации новых предложений. Повседневная
                                            практика показывает, что дальнейшее развитие различных форм деятельности
                                            позволяет оценить значение модели развития. Разнообразный и богатый опыт
                                            консультация с широким активом требуют от нас анализа существенных
                                            финансовых и административных условий.</p>

                                        <p> Товарищи! реализация намеченных плановых заданий требуют от нас анализа
                                            новых предложений. С другой стороны начало повседневной работы по
                                            формированию позиции играет важную роль в формировании систем массового
                                            участия. Идейные соображения высшего порядка, а также консультация с
                                            широким
                                            активом требуют определения и уточнения существенных финансовых и
                                            административных условий. Товарищи! новая модель организационной
                                            деятельности позволяет оценить значение соответствующий условий
                                            активизации.</p>

                                        <p> Равным образом постоянный количественный рост и сфера нашей активности
                                            представляет собой интересный эксперимент проверки соответствующий
                                            условий
                                            активизации. Товарищи! дальнейшее развитие различных форм деятельности
                                            представляет собой интересный эксперимент проверки модели развития.
                                            Повседневная практика показывает, что консультация с широким активом
                                            требуют
                                            от нас анализа систем массового участия. С другой стороны сложившаяся
                                            структура организации требуют от нас анализа направлений прогрессивного
                                            развития.</p>
                                    </div>
                                </div>
                                <div class="tabs_questions__line">
                                    <div class="tabs_questions__title">
                                        <span class="quest__plus"><i class="fas fa-plus"></i></span>
                                        <h5> Предусмотрены ли скидки для постоянных клиентов?</h5></div>
                                    <div class="tabs_questions__information" style="display: none;">
                                        <p>
                                            Повседневная практика показывает, что новая модель организационной
                                            деятельности позволяет выполнять важные задания по разработке системы
                                            обучения кадров, соответствует насущным потребностям. С другой стороны
                                            начало повседневной работы по формированию позиции способствует подготовки и
                                            реализации направлений прогрессивного развития. Значимость этих проблем
                                            настолько очевидна, что дальнейшее развитие различных форм деятельности
                                            способствует подготовки и реализации новых предложений. Повседневная
                                            практика показывает, что дальнейшее развитие различных форм деятельности
                                            позволяет оценить значение модели развития. Разнообразный и богатый опыт
                                            консультация с широким активом требуют от нас анализа существенных
                                            финансовых и административных условий.

                                        </p>
                                        <p>
                                            Товарищи! реализация намеченных плановых заданий требуют от нас анализа
                                            новых предложений. С другой стороны начало
                                            повседневной работы по формированию позиции играет важную роль в
                                            формировании систем массового участия.
                                            Идейные соображения высшего порядка, а также консультация с широким активом
                                            требуют определения и уточнения
                                            существенных финансовых и административных условий. Товарищи! новая модель
                                            организационной деятельности позволяет
                                            оценить значение соответствующий условий активизации.
                                        </p>
                                        <p>
                                            Равным образом постоянный количественный рост и сфера нашей активности
                                            представляет собой интересный эксперимент проверки соответствующий условий
                                            активизации. Товарищи! дальнейшее развитие различных форм деятельности
                                            представляет собой интересный эксперимент проверки модели развития.
                                            Повседневная практика показывает, что консультация с широким активом требуют
                                            от нас анализа систем массового участия. С другой стороны сложившаяся
                                            структура организации требуют от нас анализа направлений прогрессивного
                                            развития.
                                        </p>
                                    </div>
                                </div>
                                <div class="tabs_questions__line">
                                    <div class="tabs_questions__title">
                                        <span class="quest__plus"><i class="fas fa-plus"></i></span>
                                        <h5>Есть ли возможность совместных покупок для того, чтобы уменьшить некоторые
                                            затраты?</h5></div>
                                    <div class="tabs_questions__information" style="display: none;">
                                        <p>
                                            Повседневная практика показывает, что новая модель организационной
                                            деятельности позволяет выполнять важные задания по разработке системы
                                            обучения кадров, соответствует насущным потребностям. С другой стороны
                                            начало повседневной работы по формированию позиции способствует подготовки и
                                            реализации направлений прогрессивного развития. Значимость этих проблем
                                            настолько очевидна, что дальнейшее развитие различных форм деятельности
                                            способствует подготовки и реализации новых предложений. Повседневная
                                            практика показывает, что дальнейшее развитие различных форм деятельности
                                            позволяет оценить значение модели развития. Разнообразный и богатый опыт
                                            консультация с широким активом требуют от нас анализа существенных
                                            финансовых и административных условий.

                                        </p>
                                        <p>
                                            Товарищи! реализация намеченных плановых заданий требуют от нас анализа
                                            новых предложений. С другой стороны начало
                                            повседневной работы по формированию позиции играет важную роль в
                                            формировании систем массового участия.
                                            Идейные соображения высшего порядка, а также консультация с широким активом
                                            требуют определения и уточнения
                                            существенных финансовых и административных условий. Товарищи! новая модель
                                            организационной деятельности позволяет
                                            оценить значение соответствующий условий активизации.
                                        </p>
                                        <p>
                                            Равным образом постоянный количественный рост и сфера нашей активности
                                            представляет собой интересный эксперимент проверки соответствующий условий
                                            активизации. Товарищи! дальнейшее развитие различных форм деятельности
                                            представляет собой интересный эксперимент проверки модели развития.
                                            Повседневная практика показывает, что консультация с широким активом требуют
                                            от нас анализа систем массового участия. С другой стороны сложившаяся
                                            структура организации требуют от нас анализа направлений прогрессивного
                                            развития.
                                        </p>
                                    </div>
                                </div>
                                <div class="tabs_questions__line">
                                    <div class="tabs_questions__title">
                                        <span class="quest__plus"><i class="fas fa-plus"></i></span>
                                        <h5>Какой банковской карточкой можно оплатить товар в американском
                                            интернет-магазине?</h5></div>
                                    <div class="tabs_questions__information" style="display: none;">
                                        <p>
                                            Повседневная практика показывает, что новая модель организационной
                                            деятельности позволяет выполнять важные задания по разработке системы
                                            обучения кадров, соответствует насущным потребностям. С другой стороны
                                            начало повседневной работы по формированию позиции способствует подготовки и
                                            реализации направлений прогрессивного развития. Значимость этих проблем
                                            настолько очевидна, что дальнейшее развитие различных форм деятельности
                                            способствует подготовки и реализации новых предложений. Повседневная
                                            практика показывает, что дальнейшее развитие различных форм деятельности
                                            позволяет оценить значение модели развития. Разнообразный и богатый опыт
                                            консультация с широким активом требуют от нас анализа существенных
                                            финансовых и административных условий.

                                        </p>
                                        <p>
                                            Товарищи! реализация намеченных плановых заданий требуют от нас анализа
                                            новых предложений. С другой стороны начало
                                            повседневной работы по формированию позиции играет важную роль в
                                            формировании систем массового участия.
                                            Идейные соображения высшего порядка, а также консультация с широким активом
                                            требуют определения и уточнения
                                            существенных финансовых и административных условий. Товарищи! новая модель
                                            организационной деятельности позволяет
                                            оценить значение соответствующий условий активизации.
                                        </p>
                                        <p>
                                            Равным образом постоянный количественный рост и сфера нашей активности
                                            представляет собой интересный эксперимент проверки соответствующий условий
                                            активизации. Товарищи! дальнейшее развитие различных форм деятельности
                                            представляет собой интересный эксперимент проверки модели развития.
                                            Повседневная практика показывает, что консультация с широким активом требуют
                                            от нас анализа систем массового участия. С другой стороны сложившаяся
                                            структура организации требуют от нас анализа направлений прогрессивного
                                            развития.
                                        </p>
                                    </div>
                                </div>
                                <div class="tabs_questions__line">
                                    <div class="tabs_questions__title">
                                        <span class="quest__plus"><i class="fas fa-plus"></i></span>
                                        <h5>Могу ли я добавить еще один адрес получателя для себя, или отправить посылку
                                            другому получателю напрямую (другу, родственнику и т.д.)?</h5></div>
                                    <div class="tabs_questions__information" style="display: none;">
                                        <p>
                                            Повседневная практика показывает, что новая модель организационной
                                            деятельности позволяет выполнять важные задания по разработке системы
                                            обучения кадров, соответствует насущным потребностям. С другой стороны
                                            начало повседневной работы по формированию позиции способствует подготовки и
                                            реализации направлений прогрессивного развития. Значимость этих проблем
                                            настолько очевидна, что дальнейшее развитие различных форм деятельности
                                            способствует подготовки и реализации новых предложений. Повседневная
                                            практика показывает, что дальнейшее развитие различных форм деятельности
                                            позволяет оценить значение модели развития. Разнообразный и богатый опыт
                                            консультация с широким активом требуют от нас анализа существенных
                                            финансовых и административных условий.

                                        </p>
                                        <p>
                                            Товарищи! реализация намеченных плановых заданий требуют от нас анализа
                                            новых предложений. С другой стороны начало
                                            повседневной работы по формированию позиции играет важную роль в
                                            формировании систем массового участия.
                                            Идейные соображения высшего порядка, а также консультация с широким активом
                                            требуют определения и уточнения
                                            существенных финансовых и административных условий. Товарищи! новая модель
                                            организационной деятельности позволяет
                                            оценить значение соответствующий условий активизации.
                                        </p>
                                        <p>
                                            Равным образом постоянный количественный рост и сфера нашей активности
                                            представляет собой интересный эксперимент проверки соответствующий условий
                                            активизации. Товарищи! дальнейшее развитие различных форм деятельности
                                            представляет собой интересный эксперимент проверки модели развития.
                                            Повседневная практика показывает, что консультация с широким активом требуют
                                            от нас анализа систем массового участия. С другой стороны сложившаяся
                                            структура организации требуют от нас анализа направлений прогрессивного
                                            развития.
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="show__other__answers">
                    <a href="#" class="button__orange tabs__orange">Смотреть другие вопросы</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include("template/set-question.php") ?>
<section class="reviews">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="reviews_wrapper">
                    <div class="reviews__title">
                        <h2>Отзывы <span>наших клиентов</span></h2>
                    </div>
                    <div class="reviews__icon">
                        <img src="img/main/block9/otzuw.svg" alt="">
                    </div>
                    <div class="reviews__content">
                        <div class="reviews__slider owl-carousel">
                            <div class="reviews__slider__item">
                                <div class="reviews__slider__item__aboutPerson">
                                    <div class="reviews__slider__item__name">
                                        <h5>Cветлана</h5>
                                    </div>
                                    <div class="reviews__slider__item__date">
                                        <div class="reviews__slider__item__date-left">
                                            <p>27.05.2018</p>
                                        </div>
                                        <div class="reviews__slider__item__date-stars">
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart_2.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart_2.svg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews__slider__item__text">
                                    <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                        несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                        начинающему оратору отточить навык публичных выступлений в домашних
                                        условиях.
                                        При создании генератора мы использовали небезизвестный универсальный код
                                        речей.
                                        Текст генерируется абзацами случайным образом от двух до десяти предложений
                                        в
                                        абзаце, что позволяет сделать текст более привлекательным и живым для
                                        визуально-слухового восприятия.</p>
                                </div>
                            </div>
                            <div class="reviews__slider__item">
                                <div class="reviews__slider__item__aboutPerson">
                                    <div class="reviews__slider__item__name">
                                        <h5>Cветлана</h5>
                                    </div>
                                    <div class="reviews__slider__item__date">
                                        <div class="reviews__slider__item__date-left">
                                            <p>27.05.2018</p>
                                        </div>
                                        <div class="reviews__slider__item__date-stars">
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews__slider__item__text">
                                    <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                        несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                        начинающему оратору отточить навык публичных выступлений в домашних
                                        условиях.
                                        При создании генератора мы использовали небезизвестный универсальный код
                                        речей.
                                        Текст генерируется абзацами случайным образом от двух до десяти предложений
                                        в
                                        абзаце, что позволяет сделать текст более привлекательным и живым для
                                        визуально-слухового восприятия.</p>
                                </div>
                            </div>
                            <div class="reviews__slider__item">
                                <div class="reviews__slider__item__aboutPerson">
                                    <div class="reviews__slider__item__name">
                                        <h5>Cветлана</h5>
                                    </div>
                                    <div class="reviews__slider__item__date">
                                        <div class="reviews__slider__item__date-left">
                                            <p>27.05.2018</p>
                                        </div>
                                        <div class="reviews__slider__item__date-stars">
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart_2.svg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews__slider__item__text">
                                    <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                        несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                        начинающему оратору отточить навык публичных выступлений в домашних
                                        условиях.
                                        При создании генератора мы использовали небезизвестный универсальный код
                                        речей.
                                        Текст генерируется абзацами случайным образом от двух до десяти предложений
                                        в
                                        абзаце, что позволяет сделать текст более привлекательным и живым для
                                        визуально-слухового восприятия.</p>
                                </div>
                            </div>
                            <div class="reviews__slider__item">
                                <div class="reviews__slider__item__aboutPerson">
                                    <div class="reviews__slider__item__name">
                                        <h5>Cветлана</h5>
                                    </div>
                                    <div class="reviews__slider__item__date">
                                        <div class="reviews__slider__item__date-left">
                                            <p>27.05.2018</p>
                                        </div>
                                        <div class="reviews__slider__item__date-stars">
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart.svg" alt="">
                                            </div>
                                            <div class="star">
                                                <img src="img/main/block9/hart_2.svg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="reviews__slider__item__text">
                                    <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                        несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                        начинающему оратору отточить навык публичных выступлений в домашних
                                        условиях.
                                        При создании генератора мы использовали небезизвестный универсальный код
                                        речей.
                                        Текст генерируется абзацами случайным образом от двух до десяти предложений
                                        в
                                        абзаце, что позволяет сделать текст более привлекательным и живым для
                                        визуально-слухового восприятия.</p>
                                </div>
                            </div>
                        </div>
                        <div class="other-reviews__btn ">
                            <a href="#" class="button__orange tabs__orange">Смотреть другие отзывы</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section class="useful-now">
    <div class="container">
        <div class="useful-now__wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="useful-now__title">
                        <h2>Полезно знать</h2>
                    </div>
                    <div class="useful-now__information">
                        <div class="useful-now__news">
                            <div class="useful-now__news__fotoBlock" data-aos="zoom-in" data-aos-once="true">
                                <a href="#">
                                    <div class="useful-now__news__foto">
                                        <img src="img/main/block10/foto1.png" alt="">
                                    </div>
                                    <div class="useful-now__news__shadow">
                                    </div>
                                </a>
                            </div>
                            <div class="useful-now__titleNews">
                                <a class="useful-now__title__news" href="#">Заголовок новости, который отображает
                                    возможную длину новости или акции размером более одной строки</a>
                            </div>
                        </div>
                        <div class="useful-now__news">
                            <div class="useful-now__news__fotoBlock" data-aos="zoom-in" data-aos-once="true">
                                <a href="#">
                                    <div class="useful-now__news__foto">
                                        <img src="img/main/block10/foto2.png" alt="">
                                    </div>
                                    <div class="useful-now__news__shadow">
                                    </div>
                                </a>
                            </div>
                            <div class="useful-now__titleNews">
                                <a class="useful-now__title__news" href="#">Заголовок новости, который отображает
                                    возможную длину новости или акции размером более одной строки</a>
                            </div>
                        </div>
                        <div class="useful-now__news">
                            <div class="useful-now__news__fotoBlock" data-aos="zoom-in" data-aos-once="true">
                                <a href="#">
                                    <div class="useful-now__news__foto">
                                        <img src="img/main/block10/foto3.png" alt="">
                                    </div>
                                    <div class="useful-now__news__shadow">
                                    </div>
                                </a>
                            </div>
                            <div class="useful-now__titleNews">
                                <a class="useful-now__title__news" href="#">Заголовок новости, который отображает
                                    возможную длину новости или акции размером более одной строки</a>
                            </div>
                        </div>
                    </div>
                    <div class="useful-now__news__more__btn">
                        <a href="#" class="button__orangeDark tabs__orange">Узнать больше</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script type="text/javascript" src="/js/main/sliderSale.js"></script>
<script type="text/javascript" src="/js/main/validateMain.js"></script>
<script type="text/javascript" src="/js/fancyboxMain.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/discount/discount.js"></script>
<script type="text/javascript" src="/js/discount/timerAll.js"></script>
<script type="text/javascript" src="/js/discount/timerAll_small.js"></script>
<?php include("template/popupMain.php") ?>
<?php include("template/footer.php") ?>


</body>
</html>