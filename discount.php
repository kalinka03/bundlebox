<?php include("template/header-main.php") ?>
<?php //include("template/header-lk.php") ?>

<section class="promotions-discounts">
    <div class="promotions-discounts__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 promotions-discounts__content">
                    <div class="col-lg-6 col-xs12">
                        <h2>Акции и скидки</h2>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="promotions-discounts__fotoGirl">
                            <img src="img/discount/fon_girls.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 promotions-discounts__content">
                    <div class="promotions-discounts__button">
                        <div class="promotions-discounts__buttonShop">
                            <a class="button__orangeBr" href="#">Каталог магазинов </a>
                        </div>
                        <div class="promotions-discounts__buttonDiscount">
                            <a class="button__redBag btn_catalogShop btn_akcii_active disabled" href="#">Акции</a>
                        </div>
                        <div class="promotions-discounts__buttonCompany">
                            <a class="button__redBag btn_catalogShop btn_discountCatalog" href="#">Компания на скидку </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form action="/" class="sortDiscount">
                        <div class="promotions-discounts__sort">
                            <div class="promotions-discounts__sort__title">
                                <p>Сортировать:</p>
                            </div>
                            <div class="promotions-discounts__sort__allshops">
                                <select name="sort_shops">
                                    <option selected disabled>Все магазины</option>
                                    <option value="">Женская одежда</option>
                                    <option value="">Мужская одежда</option>
                                    <option value="">Детская одежда</option>
                                    <option value="">Игрушки</option>
                                    <option value="">Техника</option>
                                    <option value="">Товары для дома</option>
                                    <option value="">Другое</option>
                                </select>
                            </div>
                            <div class="promotions-discounts__sort__date">
                                <div class="item">
                                    <label>
                                        <input class="radio" type="radio" name="radio-test"
                                               value="По времени окончания" checked>
                                        <span class="radio-custom"></span>
                                        <span class="label">По времени окончания</span>
                                    </label>
                                </div>

                            </div>
                            <div class="promotions-discounts__sort__discont">
                                <div class="item">
                                    <label>
                                        <input class="radio" type="radio" name="radio-test"
                                               value="По размеру скидки">
                                        <span class="radio-custom"></span>
                                        <span class="label">По размеру скидки</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="promotions-discounts__some">
    <div class="container">
        <div data-aos="fade-up"  data-aos-duration="600" class="row  promotions-discounts__someItem">
            <div class="persent">
                <p>-50%</p>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__allFoto">
                    <a href="#">
                        <div class="promotions-discounts__someItem__foto">
                            <img src="img/discount/foto1.png" alt="">
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__information">
                    <a href="#">
                        <h4>Заголовок акции, который отображает возможную длину новости или акции размером
                            более
                            одной строки</h4>
                    </a>
                    <a href="#">
                        <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между
                            покупкой в рунете и за рубежом становится незаметной. В минимальные сроки посылка будет
                            доставлена по указанному адресу. Разница между
                            покупкой в рунете и за рубежом становится незаметной. В минимальные сроки посылка будет
                            доставлена по указанному адресу. Разница между
                            покупкой в рунете и за рубежом становится незаметной.</p>
                    </a>
                    <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>
                    <div class="slider-akcii__content__timer">
                        <div class="DateTimer" id="timer5" datetime="August 30, 2018 13:51:50"></div>
                    </div>
                    <div class="go_shop_discount">
                        <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>
                    </div>
                </div>
            </div>
        </div>
        <div data-aos="fade-up"  data-aos-duration="600" class="row  promotions-discounts__someItem">
            <div class="persent">
                <p>-40%</p>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__allFoto">
                    <a href="#">
                        <div class="promotions-discounts__someItem__foto">
                            <img src="img/discount/foto2.png" alt="">
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__information">
                    <a href="#">
                        <h4>Заголовок акции, который отображает возможную длину новости или акции размером
                        </h4>
                    </a>
                    <a href="#">
                        <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между
                            покупкой в рунете и за рубежом становится незаметной. </p>
                    </a>
                    <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>
                    <div class="slider-akcii__content__timer">
                        <div class="DateTimer" id="timer6" datetime="August 25, 2018 13:51:50"></div>
                    </div>
                    <div class="go_shop_discount">
                        <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>
                    </div>
                </div>
            </div>
        </div>
        <div data-aos="fade-up"  data-aos-duration="600" class="row  promotions-discounts__someItem">
            <div class="persent">
                <p>-40%</p>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__allFoto">

                    <a href="#">
                        <div class="promotions-discounts__someItem__foto">
                            <img src="img/discount/foto3.png" alt="">
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__information">
                    <a href="#">
                        <h4>Заголовок акции, который отображает возможную длину новости или акции размером
                        </h4>
                    </a>
                    <a href="#">
                        <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между
                            покупкой в рунете и за рубежом становится незаметной. </p>
                    </a>
                    <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>
                    <div class="slider-akcii__content__timer">
                        <div class="DateTimer" id="timer7" datetime="September 25, 2018 13:51:50"></div>
                    </div>
                    <div class="go_shop_discount">
                        <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>
                    </div>
                </div>
            </div>
        </div>
        <div data-aos="fade-up"  data-aos-duration="600" class="row  promotions-discounts__someItem">
            <div class="persent">
                <p>-40%</p>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__allFoto">

                    <a href="#">
                        <div class="promotions-discounts__someItem__foto">
                            <img src="img/discount/foto4.png" alt="">
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__information">
                    <a href="#">
                        <h4>Заголовок акции, который отображает возможную длину новости или акции размером
                        </h4>
                    </a>
                    <a href="#">
                        <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между
                            покупкой в рунете и за рубежом становится незаметной. </p>
                    </a>
                    <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>
                    <div class="slider-akcii__content__timer">
                        <div class="DateTimer" id="timer8" datetime="September 25, 2018 13:51:50"></div>
                    </div>
                    <div class="go_shop_discount">
                        <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>
                    </div>
                </div>
            </div>
        </div>
        <div data-aos="fade-up"  data-aos-duration="600" class="row  promotions-discounts__someItem">
            <div class="persent">
                <p>-20%</p>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__allFoto">

                    <a href="#">
                        <div class="promotions-discounts__someItem__foto">
                            <img src="img/discount/foto5.png" alt="">
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="promotions-discounts__someItem__information">
                    <a href="#">
                        <h4>Заголовок акции, который отображает возможную длину новости или акции размером
                        </h4>
                    </a>
                    <a href="#">
                        <p>В минимальные сроки посылка будет доставлена по указанному адресу. Разница между
                            покупкой в рунете и за рубежом становится незаметной. </p>
                    </a>
                    <h5>ДО ЗАВЕРШЕНИЯ АКЦИИ ОСТАЛОСЬ</h5>
                    <div class="slider-akcii__content__timer">
                        <div class="DateTimer" id="timer9" datetime="September 25, 2018 13:51:50"></div>
                    </div>
                    <div class="go_shop_discount">
                        <a href="#" class="button__orange tabs__orange">Перейти в магазин</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="line_discount">
        </div>
    </div>
</section>
<section class="section-pagination">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-pagination__content">
                    <a class="arrow-left" href="#">
                        <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             x="0px"
                             y="0px"
                             viewBox="0 0 15.6 15.5" style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                        <style type="text/css">
                            .st0 {
                                fill: #434343;
                            }
                        </style>
                            <g>
                                <g>
                                    <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                        			c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                        			C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                </g>
                            </g>
                            </g>
                        </svg>
                    </a>
                    <a class="active number" href="#">1</a>
                    <a class="number" href="#">2</a>
                    <a class="number" href="#">3</a>
                    <a class="more" href="#">...</a>
                    <a class="number" href="#">12</a>
                    <a class="number" href="#">13</a>
                    <a class="arrow-right" href="#">
                        <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             x="0px"
                             y="0px"
                             viewBox="0 0 15.6 15.5" style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                        <style type="text/css">
                            .st0 {
                                fill: #434343;
                            }
                        </style>
                            <g>
                                <g>
                                    <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                        			c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                        			C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                </g>
                            </g>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="/js/discount/discount.js"></script>
<script type="text/javascript" src="/js/discount/timerAll.js"></script>
<script type="text/javascript" src="/js/fancyboxMain.js"></script>
<script type="text/javascript" src="/js/main/validateMain.js"></script>
<?php include("template/popupMain.php") ?>
<?php include("template/footer.php") ?>
