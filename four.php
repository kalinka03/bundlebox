<?php include("template/header-main.php") ?>
<link rel="stylesheet" href="plugin/fontawesome-free-5.1.0-web/css/all.css">
<link rel="stylesheet" href="css/four/four.css">
<link rel="stylesheet" href="css/four/four__media.css">


<div class="four">
    <div class="container four__wrap">
        <div class="col-12">
            <div class="four__content">
                <div class="four__info">
                    <div class="col-4">
                        <div class="four__info-title">
                            <h2>
                                Страница
                                не найдена
                            </h2>
                        </div>
                        <div class="four__info-link">
                            <a href="#">
                                Перейти на главную страницу
                            </a>
                            <a href="#">
                                Перейти в личный кабинет
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include("template/footer.php") ?>
<?php include("template/popupMain.php") ?>


</body>
</html>