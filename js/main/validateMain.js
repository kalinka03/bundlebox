$(function () {
    Validate_main();
    mask_Phone();

})


function Validate_main() {
        // $("form").validate({
        //     rules: {
        //         name: {
        //             required: true,
        //         },
        //         surname: {
        //             required: true,
        //         },
        //         email: {
        //             required: true,
        //             email: true
        //         },
        //         phone: {
        //             required: true,
        //         }
        //
        //     },
        //     messages: {
        //         name: {
        //             required: "Заполните поле",
        //         },
        //         email: {
        //             required: "Заполните поле",
        //             email: "Введите  корректный адрес",
        //         },
        //         phone: {
        //             required: "Заполните поле",
        //         }
        //
        //     }
        // });
    $("#js_enterSite form").validate({
        rules: {
            name: {
                required: true,
            },

            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                regex: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/g,
            }

        },
        messages: {
            name: {
                required: "Заполните поле",
            },
            email: {
                required: "Заполните поле",
                email: "Введите  корректный адрес",
            },
            password: {
                required: "Заполните поле",
                regex: "Пароль не правильный",
            }

        }
    });
    }

function mask_Phone(){
    $(".maskPhone").mask("8(999) 999-9999");

}

