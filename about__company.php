<?php include("template/header-main.php") ?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
      integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

<!--<link rel="stylesheet" href="css/rate/rate.css">-->
<link rel="stylesheet" href="css/about__company/about__company.css">
<link rel="stylesheet" href="css/about__company/about__company-media.css">


<div class="about__company">
    <div class="about__top ">
        <div class="container about__top-wrap">
            <div class="row ">
                <div class="about__img-wrap">
                    <div class="about__img1">
                        <img src="img/about__company/about1.png" alt="photo">
                    </div>
                    <div class="about__img2 wow fadeInLeft"
                         data-wow-offset="200"
                         data-wow-delay="0.5s"
                         data-wow-duration="2s">
                        <img src="img/about__company/about2.png" alt="photo">
                    </div>
                    <div class="about__img3 wow fadeInLeft"
                         data-wow-offset="200"
                         data-wow-delay="1.5s"
                         data-wow-duration="2s">
                        <img src="img/about__company/car.png" alt="photo">
                    </div>
                </div>
                <div class="col-12">
                    <div class="about__title">
                        <h2 >
                            О компании
                        </h2>
                        <div class="about__all-photo">
                            <img src="img/about__company/about__all.png" alt="photo">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="about__block2">
        <div class="about__block2-wrap container">
            <div class="block2__title">
                <p><span>BUNDLE <span class="about__box">BOX</span></span> - легкая интеграция в мировой шопинг</p>
            </div>
            <div class="row justify-content-center">
                <div class="block2__anim col-lg-8 col-12">
                    <div class="block2__anim-item">
                        <div class="anim__icon wow bounceInLeft"
                             data-wow-offset="200"
                             data-wow-delay="0.2s"
                             data-wow-duration="2s">
                            <img src="img/about__company/icon1.png" alt="icon">
                        </div>
                        <div class="anim__text wow fadeIn"
                             data-wow-offset="200"
                             data-wow-delay="0.2s"
                             data-wow-duration="2s">
                            <span>
                                Легко
                            </span>
                        </div>
                    </div>
                    <div class="block2__arrow wow fadeIn"
                         data-wow-offset="200"
                         data-wow-delay="0.4s"
                         data-wow-duration="2s">
                        <img src="img/about__company/Arrow.svg" alt="arrow">
                    </div>
                    <div class="block2__anim-item">
                        <div class="anim__icon wow bounceInLeft"
                             data-wow-offset="200"
                             data-wow-delay="0.4s"
                             data-wow-duration="2s">
                            <img src="img/about__company/icon2.png" alt="icon">
                        </div>
                        <div class="anim__text wow fadeIn"
                             data-wow-offset="200"
                             data-wow-delay="0.4s"
                             data-wow-duration="2s">
                            <span>
                                Быстро
                            </span>
                        </div>
                    </div>
                    <div class="block2__arrow wow fadeIn"
                         data-wow-offset="200"
                         data-wow-delay="0.6s"
                         data-wow-duration="2s">
                        <img src="img/about__company/Arrow.svg" alt="arrow">
                    </div>
                    <div class="block2__anim-item">
                        <div class="anim__icon wow bounceInLeft"
                             data-wow-offset="200"
                             data-wow-delay="0.6s"
                             data-wow-duration="2s">
                            <img src="img/about__company/icon3.png" alt="icon">
                        </div>
                        <div class="anim__text wow fadeIn"
                             data-wow-offset="200"
                             data-wow-delay="0.6s"
                             data-wow-duration="2s">
                            <span>
                                Дешево
                            </span>
                        </div>
                    </div>
                    <div class="block2__arrow wow fadeIn"
                         data-wow-offset="200"
                         data-wow-delay="0.8s"
                         data-wow-duration="2s">
                        <img src="img/about__company/Arrow.svg" alt="arrow">
                    </div>
                    <div class="block2__anim-item">
                        <div class="anim__icon wow bounceInLeft"
                             data-wow-offset="200"
                             data-wow-delay="0.8s"
                             data-wow-duration="2s">
                            <img src="img/about__company/icon4.png" alt="icon">
                        </div>
                        <div class="anim__text wow fadeIn"
                             data-wow-offset="200"
                             data-wow-delay="0.8s"
                             data-wow-duration="2s">
                            <span>
                                Надежно
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="block2__bundlebox-left  ">
                    <span>BUNDLE</span>
                </div>
                <div class="block2__infotext col-lg-10 offset-lg-1">
                    <p><span>BUNDLE <span class="box__color">BOX</span></span> – простая и выгодная организация доставки товаров из США и стран Европы для российских покупателей. Мы предлагаем совершать шопинг заграницей, не выходя из дома и выступаем связующим звеном, которое делает товары мировых брендов легкодоступными. Bundle Box отвечает за то, чтобы Вы получали свои заказы из США и Европы в полном объеме, без повреждений и в срок. В Иркутской области организована сеть офисов выдачи заказов, что делает получение посылок еще проще и удобней.
                    </p>
                    <p>
                        Мы создаем все условия чтобы наши клиенты остались довольны. Доставка товаров из США и Европы с Bundle Box становится быстрой и доступной, как никогда!
                    </p>
                </div>
                <div class="block2__bundlebox-right col-lg-1 ">
                    <span>BOX</span>
                </div>
            </div>

        </div>
    </div>
    <div class="mission">
        <div class="container">
            <div class="col-6 mission__img">
                <div class="mission__img-4 wow bounceInLeft" data-wow-offset="200"
                     data-wow-delay="0.2s"
                     data-wow-duration="2s">
                    <img src="img/about__company/miss4.png" alt="img">
                </div>
                <div class="mission__img-3 wow bounceInLeft" data-wow-offset="200"
                     data-wow-delay="0.4s"
                     data-wow-duration="2s">
                    <img src="img/about__company/miss3.png" alt="img">
                </div>
                <div class="mission__img-2 wow bounceInLeft" data-wow-offset="200"
                     data-wow-delay="0.6s"
                     data-wow-duration="2s">
                    <img src="img/about__company/miss2.png" alt="img">
                </div>
                <div class="mission__img-1 wow bounceInLeft" data-wow-offset="200"
                     data-wow-delay="0.8s"
                     data-wow-duration="2s">
                    <img src="img/about__company/miss1.png" alt="img">
                </div>
            </div>
            <div class=" row  mission__content align-items-center ">
               <div class="col-md-6 col-12 ">
                   <div class="mission__img-all">
                       <img src="img/about__company/missall.png" alt="img">
                   </div>
               </div>
                <div class="col-md-6 col-12 mission__info ">
                    <h2>
                        Наша миссия:
                    </h2>
                    <ul>
                        <li>стереть границы шопинга</li>
                        <li>сделать качество доступным дешево</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="about__page container">
        <div class="about__page-logo wow bounceInDown" data-wow-offset="200"
             data-wow-delay="0.2s"
             data-wow-duration="2s" >
            <p>О нас</p>
        </div>
        <div class="row justify-content-center">
                <div class="col-md-4  col-sm-6 col-12">
                    <div class="about__square wow fadeInDown" data-wow-offset="200"
                         data-wow-delay="0.2s"
                         data-wow-duration="2s">
                        <div class="square__icon">
                            <img src="img/about__company/Earth.svg" alt="earth">
                        </div>
                        <div class="square__text">
                            <h2>
                                10 лет опыта
                            </h2>
                            <p>
                                Более 10 лет в сфере организации покупок и доставки посылок из США и Европы – мы очень хорошо знаем свою работу.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4  col-sm-6 col-12">
                    <div class="about__square wow fadeInDown" data-wow-offset="200"
                         data-wow-delay="0.4s"
                         data-wow-duration="2s">
                        <div class="square__icon">
                            <img src="img/about__company/People.svg" alt="earth">
                        </div>
                        <div class="square__text">
                            <h2>
                                Тысячи довольных клиентов
                            </h2>
                            <p>
                                За годы работы с нами тысячи клиентов убедились в том, что мы предлагаем очень выгодные условия.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4  col-sm-6 col-12">
                    <div class="about__square wow fadeInDown" data-wow-offset="200"
                         data-wow-delay="0.6s"
                         data-wow-duration="2s">
                        <div class="square__icon">
                            <img src="img/about__company/Cargo.svg" alt="earth">
                        </div>
                        <div class="square__text">
                            <h2>
                                Тонны доставленных товаров
                            </h2>
                            <p>
                                Доставка покупок из США и Европы это не просто перемещение объекта из пункта А в пункт Б. Мы оптимизируем этот процесс и делаем максимально удобным и выгодным для наших клиентов.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="about__boon ">
        <div class="container ">
            <div class="about__boon-wrap">
                <div class="about__boon-img">
                    <div class="about__boon-left wow fadeInLeft" data-wow-offset="200"
                         data-wow-delay="0.2"
                         data-wow-duration="2s">
                        <img src="img/about__company/boon__left.png" alt="img">
                    </div>
                </div>
                <div class="boon__content row align-items-lg-center align-items-start ">
                    <div class="boon__block col-md-4 col-sm-6 col-12">
                        <h2>
                            Простая схема обслуживания
                        </h2>
                        <p>
                            Мы предлагаем несколько способов организации покупки. Это гарантирует гибкость и удобство процесса шопинга. Вы можете покупать что угодно и как угодно, а мы это доставим.
                        </p>
                    </div>
                    <div class="boon__block col-md-3 col-sm-6 col-12">
                        <h2>
                            Удобство доставки
                        </h2>
                        <p>
                            Любую вашу посылку из США и Европы мы доставим  по выгодным тарифам. Вы  оплачиваете только доставку и получаете свою посылку по указанному адресу.
                        </p>
                    </div>
                    <div class="boon__block-img-right col-md-5 col-12">
                        <div class="about__boon-right1 wow fadeInRight" data-wow-offset="200"
                             data-wow-delay="0.2s"
                             data-wow-duration="2s">
                            <img src="img/about__company/boon__right1.png" alt="img">
                        </div>
                        <div class="about__boon-right2 wow fadeInRight" data-wow-offset="200"
                             data-wow-delay="0.4s"
                             data-wow-duration="2.5s">
                            <img src="img/about__company/boon__right2.png" alt="img">
                        </div>
                        <div class="about__boon-right3 wow fadeInRight" data-wow-offset="200"
                             data-wow-delay="0.6s"
                             data-wow-duration="3s">
                            <img src="img/about__company/boon__right3.png" alt="img">
                        </div>
                        <div class="about__boon-all">
                            <img src="img/about__company/boon__rightAll.png" alt="img">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="about__reviews">
        <section class="reviews ">
            <div class="container about__reviews-wrap">
                <div class="about__reviews-logo wow bounceInDown" data-wow-offset="200"
                     data-wow-delay="0.2s"
                     data-wow-duration="2s">
                    <p>
                        Отзывы
                    </p>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="reviews_wrapper">
                            <div class="reviews__title">
                                <h2>Отзывы <span>наших клиентов</span></h2>
                                <div class="reviews__icon">
                                    <img src="img/main/block9/otzuw.svg" alt="">
                                </div>
                                <div class="reviews__content">
                                    <div class="reviews__slider owl-carousel owl-loaded owl-drag">
                                        <div class="owl-stage-outer owl-height" style="height: 333px;"><div class="owl-stage" style="transform: translate3d(-1860px, 0px, 0px); transition: all 0s ease 0s; width: 3100px;"><div class="owl-item cloned" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div><div class="owl-item cloned" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart_2.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div><div class="owl-item cloned" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart_2.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div><div class="owl-item" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart_2.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart_2.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div><div class="owl-item" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div><div class="owl-item" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart_2.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div><div class="owl-item active" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart_2.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div><div class="owl-item cloned active" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart_2.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart_2.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div><div class="owl-item cloned active" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div><div class="owl-item cloned" style="width: 310px;"><div class="reviews__slider__item">
                                                        <div class="reviews__slider__item__aboutPerson">
                                                            <div class="reviews__slider__item__name">
                                                                <h5>Cветлана</h5>
                                                            </div>
                                                            <div class="reviews__slider__item__date">
                                                                <div class="reviews__slider__item__date-left">
                                                                    <p>27.05.2018</p>
                                                                </div>
                                                                <div class="reviews__slider__item__date-stars">
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart.svg" alt="">
                                                                    </div>
                                                                    <div class="star">
                                                                        <img src="img/main/block9/hart_2.svg" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="reviews__slider__item__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать
                                                                несколько абзацев более менее осмысленного текста рыбы на русском языке, а
                                                                начинающему оратору отточить навык публичных выступлений в домашних
                                                                условиях.
                                                                При создании генератора мы использовали небезизвестный универсальный код
                                                                речей.
                                                                Текст генерируется абзацами случайным образом от двух до десяти предложений
                                                                в
                                                                абзаце, что позволяет сделать текст более привлекательным и живым для
                                                                визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev"></button><button type="button" role="presentation" class="owl-next"></button></div><div class="owl-dots"><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot active"><span></span></button></div></div>
                                    <div class="other-reviews__btn ">
                                        <a href="#" class="button__orange tabs__orange">Смотреть другие отзывы</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div></section>
    </div>
    <div class="just">
        <div class="container">
            <div class="row just__wrap justify-content-lg-center  justify-content-md-start">
                <div class="just__img-left just__img wow fadeInLeft" data-wow-offset="200"
                     data-wow-delay="0.2s"
                     data-wow-duration="2s" >
                    <img src="img/about__company/just__left.png" alt="img">
                </div>
                <div class="just__img-all">
                    <img src="img/about__company/just__right.png" alt="img">
                </div>
                <div class="col-lg-4 col-md-6 col-12 align-self-center">
                    <div class="just__info">
                        <h2>
                            Покупать в США
                            и Европе просто!
                        </h2>
                        <div class="just__btn ">
                            <a href="#" class="button__black ">Начать шопинг</a>
                        </div>
                    </div>
                </div>
                <div class="just__img-right just__img wow fadeInRight" data-wow-offset="200"
                     data-wow-delay="0.4s"
                     data-wow-duration="2s">
                    <img src="img/about__company/just__right.png" alt="img">
                </div>

            </div>
        </div>
    </div>
    <div class="about__contacts">
        <div class="container about__contacts-container "  >
            <div class="about__contacts-logo wow bounceInDown" data-wow-offset="200"
                 data-wow-delay="0.2s"
                 data-wow-duration="2s">
                <p>
                    Контакты
                </p>
            </div>
            <div class="about__contacts-title">
                <h2>
                    Наши контакты
                </h2>
            </div>
            <div class="row ">
                <div class="col-md-4 col-12">
                    <div class="contacts__info" id="contact__info">
                        <div class="contacts__info-title">
                            <h2>Наш офис в Иркутске</h2>
                        </div>
                        <div class="contacts__info-wrap contacts__info-office">
                            <p>Улица, номер дома, этаж
                                и то, что еще надо указать </p>
                        </div>
                        <div class="contacts__info-flex">
                            <div class="contacts__info-flex-left">
                                <div class="contacts__info-wrap contacts__info-phone">
                                    <div class="info__phone-icon wow fadeInDownBig" data-wow-offset="200"
                                         data-wow-delay="0.1s"
                                         data-wow-duration="2s">
                                        <img src="img/about__company/icon__contact/Phone.svg" alt="img">
                                    </div>
                                    <div class="info__phone-list contact__info-text">
                                        <div class="info__phone-item">
                                            <a href="tel:+73952123456">
                                                <span>Телефон:</span> +7 (3952) 123456
                                            </a>
                                        </div>
                                        <div class="info__phone-item">
                                            <a href="tel:+73952123456">
                                                <span>Telegram:</span> +7 (3952) 123456
                                            </a>
                                        </div>
                                        <div class="info__phone-item">
                                            <a href="tel:+73952123456">
                                                <span>Viber:</span> +7 (3952) 123456
                                            </a>
                                        </div>
                                        <div class="info__phone-item">
                                            <a href="tel:+73952123456">
                                                <span>Whatsapp:</span> +7 (3952) 123456
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="contacts__info-flex-right">
                                <div class="contacts__info-wrap contacts__info-mail">
                                    <div class="info__mail-icon wow fadeInDownBig" data-wow-offset="200"
                                         data-wow-delay="0.2s"
                                         data-wow-duration="2s">
                                        <img src="img/about__company/icon__contact/Mail.svg" alt="img">
                                    </div>
                                    <div class="info__mail-list contact__info-text">
                                        <div class="info__phone-item">
                                            <a href="mailto:mail@email.ru">
                                                Email: mail@email.ru
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="contacts__info-wrap contacts__info-clock">
                                    <div class="info__clock-icon wow fadeInDownBig" data-wow-offset="200"
                                         data-wow-delay="0.3s"
                                         data-wow-duration="2s">
                                        <img src="img/about__company/icon__contact/Clock.svg" alt="img">
                                    </div>
                                    <div class="info__clock-list contact__info-text">
                                        <p>График работы</p>
                                        <p>Пн-Пт: 10:00 - 18:00</p>
                                        <p>Сб-Вс: выходной</p>
                                    </div>
                                </div>
                                <div class="contacts__info-wrap contacts__info-delivery">
                                    <div class="info__delivery-icon wow fadeInDownBig" data-wow-offset="200"
                                         data-wow-delay="0.4s"
                                         data-wow-duration="2s">
                                        <img src="img/about__company/icon__contact/Geolocation.svg" alt="img">
                                    </div>
                                    <div class="info__delivery-list contact__info-text">
                                        <a href="#js__contact" class="modal-trigger">Адреса офисов выдачи
                                            заказов</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-12">
                    <div class="contacts__map">
                        <div id="map__about">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about__questions">
        <div class="set-question">
            <div class="set-question__line module wow animated" data-wow-offset="200" style="visibility: visible; animation-name: increase;">
            </div>
            <!--    <canvas id="paper"></canvas>-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="set-question__wrapper wow fadeInRight animated" data-wow-offset="400" data-wow-delay="0.4s" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.4s; animation-name: fadeInRight;">
                            <h3>Если вы не нашли ответ на свой вопрос,</h3>
                            <h4>нужна консультация или рекомендация,
                                всегда рады помочь </h4>
                            <div class="set-question__popup">
                                <a href="#js_answer" class="button__white modal-trigger">Задать вопрос</a>
                            </div>
                            <div class="set-question__girl">
                                <img src="img/main/block8/girl.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("template/popup__about-company.php") ?>
<?php include("template/footer.php") ?>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnmv8rDSzIL4RVE5EBt6Kt4hqdFXU2mA4&callback=initMap">
</script>
<script type="text/javascript" src="/js/fancyboxMain.js"></script>
<script src="js/about__company/about__company.js"></script>

</body>
</html>