<?php include("template/header.php") ?>
<div class="header-lk">
    <div class="container">
        <div class="row d-flex  flex-row">
            <div class="col-lg-1 col-md-4  header_reverse">
                <a href="/">
                <div class="header__logo first_variant">
                    <img src="/img/main/SVG_logo/LOGO_wh_bg.svg" alt="">
                </div>
<!--                    <div class="header__logo lk_two_variant">-->
<!--                        <img src="/img/main/SVG_logo/LOGO_wh_bg.svg" alt="">-->
<!--                    </div>-->
                </a>
            </div>
            <div class="col-lg-11 col-md-12 media_col">
                <div class="row">
                    <div class="col  header__logo__lk">
                        <div class="header__login-interpreter">
                            <div class="header__interpreter">
                                <a href="#" class="button__orange">Сделать заказ</a>
                            </div>
                            <div class="header__login header__login__lk">
                                <div class="header__man-black">
                                    <img src="/img/main/menu/Cabinet_out.svg" alt="">
                                </div>
                                <div class="header__man-orange">
                                    <img src="/img/main/menu/Cabinet_in_oranj.svg" alt="">
                                </div>
                                <div class="header__hint">
                                    <p>Выход из личного кабинета</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100"></div>
                    <div class="col align-self-center">
                        <input class="checkbox-toggle" type="checkbox"/>
                        <div class="hamburger">
                            <div>
                                <svg viewBox="0 0 800 600" class="">
                                    <path d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200"
                                          class="top_bar"/>

                                    <path d="M300,320 L540,320" class="middle_bar"/>

                                    <path d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190"
                                          class="bottom_bar"
                                          transform="translate(480, 320) scale(1, -1) translate(-480, -318)"/>
                                </svg>
                            </div>
                        </div>
                        <div class="header__menu">
                            <div>
                                <div>
                                    <div class="info_lk_balanse">
                                        <div class="score">
                                            <h4>К оплате: <span>1005000 </span> RUB</h4>
                                        </div>
                                        <div class="balance">
                                            <h4>Баланс: <span> 10000.56 </span> RUB</h4>
                                        </div>
                                        <div class="refill">
                                            <a href="#">Пополнить счет</a>
                                        </div>
                                    </div>
                                    <ul>
                                        <li><a class="header__menu_active" href="/">Главная</a></li>
                                        <li><a class="header__menu_catalog" href="#">Каталог магазинов</a></li>
                                        <li><a href="/discount.php" class="header__menu_akcii">Акции</a></li>
                                        <li><a href="#" class="header__menu_akcii header__menu_akcii_company">Компания на скидку</a></li>
                                        <li><a class="header__menu_tarife" href="#">Тарифы</a></li>
                                        <li><a class="header__menu_quest" href="#">Вопросы</a></li>
                                        <li><a class="header__menu_news" href="#">Новости</a></li>
                                        <li><a class="header__menu_rewiews" href="#">Отзывы</a></li>
                                        <li><a class="header__menu_company" href="#">О компании</a></li>
                                    </ul>
                                    <ul class="header__menu__lk">
                                        <li><a class="header__menu_profile" href="#">Профиль</a></li>
                                        <li><a class="header__menu_order" href="#">Заказы</a></li>
                                        <li><a class="header__menu_parcels" href="#">Посылки</a></li>
                                        <li><a class="header__menu_bookkeeping" href="#">Бухгалтерия</a></li>
                                        <li><a class="header__menu_sms" href="#">Сообщения</a></li>
                                        <li><a class="header__menu_tabs" href="#">Закладки</a></li>
                                    </ul>
                                    <div class="header__interpreter mob_button_header">
                                        <a href="/delivery/order_track.php" class="button__orange">Сделать заказ</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="header_revers_mob">
                <a href="/">
                <div class="header__logo">
                    <img src="/img/main/SVG_logo/LOGO_wh_bg.svg" alt="">
                </div>
                </a>
            </div>
        </div>
    </div>

</div>
