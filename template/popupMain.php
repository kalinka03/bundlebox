<div id="js_answer" class="popup_all">
    <div class="popup_all__title">
        <h2>Задать вопрос</h2>
    </div>
    <div class="popup_all__line"></div>
    <div class="popup_all__descript">
        <p>Для получения подробной информации заполните форму и наш менеджер свяжется с Вами в ближайшее время</p>
    </div>

    <div class="popup_all__form">
        <form class="form-all form-group">
            <div class="center-input">
                <input type="hidden" name="val" value="Задать вопрос">
                <input class="popup-call__name" type="text" name="name" placeholder="Ваше имя">
                <input class="popup-call__phone maskPhone" type="text" name="phone" placeholder="Номер телефона">
                <input class="popup-call__email" type="email" name="email" placeholder="Email">
                <textarea class="popup-picking__text" rows="3" name="commit"
                          placeholder="Текст комментария"></textarea>
                <div class="btn__form ">
                    <button class="button__orange form__orange"> Отправить</button>

                </div>
            </div>
        </form>
    </div>
</div>



<div id="js_enterSite" class="popup_all">
    <div class="popup_all__title">
        <h2>Вход на сайт</h2>
    </div>
    <div class="popup_all__line"></div>
    <div class="popup_all__descript">
        <p>Для получения подробной информации заполните форму и наш менеджер свяжется с Вами в ближайшее время</p>
    </div>

    <div class="popup_all__form">
        <form class="form-all form-group">
            <div class="center-input">

                <input class="popup-call__email" type="email" name="email" placeholder="Email">
                <input class="popup-call__phone" type="text" name="password" placeholder="Пароль">
                <div class="btn__form ">
                    <button class="button__orange form__orange"> Войти</button>
                </div>
                <div class="forgot__passwordORregister">
                    <div class="forgot__password">
                        <a href="#">Забыли пароль</a>
                    </div>
                    <div class="forgot__register">
                        <a href="#">Регистрация</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<div id="js_agreement" class="popup_all">
    <div class="popup_all__title">
        <h2> Пользовательское соглашение</h2>
    </div>
    <div class="popup_all__line"></div>
    <div class="popup_agreement__descript">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor
            sit amet lacus accumsan et viverra justo to commodo. Proin sodales pulvinar tempor. Cum sociis natoque
            penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
            vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida
            dolor sit amet lacus accumsan et viverra justo in commodo. Proin sodales pulvinar tempor. Cum sociis natoque
            penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
            vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</p>
        <h4> Пользовательское соглашение</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor
            sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque
            penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
            vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </p>
        <p>
            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla
            luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
        </p>
        <p>
            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla
            luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
        </p>


    </div>

</div>

<div id="js_shop" class="popup_all">

    <div class="js_shop__logo">
        <img src="/img/popUps/logo.png" alt="">
        <div class="line"></div>
        <h4>Название магазина</h4>
    </div>
    <div class="js_shop__logo__site">
        <a href="#">sitedomain.com</a>
    </div>
    <div class="js_shop__logo__tags">
        <div class="js_shop__logo__tags__block">
            <p>название тега</p>
        </div>
        <div class="js_shop__logo__tags__block">
            <p>название длинного тега</p>
        </div>
        <div class="js_shop__logo__tags__block">
            <p>название</p>
        </div>
        <div class="js_shop__logo__tags__block">
            <p>название оч</p>
        </div>
        <div class="js_shop__logo__tags__block">
            <p>название длинного тега</p>
        </div>
        <div class="js_shop__logo__tags__block">
            <p>название</p>
        </div>
        <div class="js_shop__logo__tags__block">
            <p>название оч</p>
        </div>
        <div class="js_shop__logo__tags__block">
            <p>название тега</p>
        </div>
        <div class="js_shop__logo__tags__block">
            <p>название тега</p>
        </div>
    </div>
    <div class="js_shop__logo__descript">
        <h4>Блок текстового описания условий работы магазина или указание причин почему магазин несовместим для покупок.
            Количество текста может быть произвольным, задается администратором при добавлении магазина через админ
            панель</h4>
        <h4>Блок текстового описания условий работы магазина или указание причин почему магазин несовместим для покупок.
            Количество текста может быть произвольным, задается администратором при добавлении магазина через админ
            панель</h4>
    </div>

</div>
<div id="js_agreement" class="popup_all">
    <div class="popup_all__title">
        <h2> Пользовательское соглашение</h2>
    </div>
    <div class="popup_all__line"></div>
    <div class="popup_agreement__descript">
        <p>Lorem aretra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</p>
        <h4> Пользовательское соглашение</h4>
        <p>Lorem ipsuNam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc
            eget.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </p>
        <p>
            Cum sociis natoque penllis orci, sed rhoncus sapien nunc eget.
        </p>
        <p>is tellus mollis orci, sed rhoncus sapien nunc eget.
        </p>
    </div>
</div>





<div id="js__thank" class="popup_all rate__popup-after">
    <div class="popup_all__title">
        <h2>Ваша заявка принята</h2>
    </div>
    <div class="popup_all__line"></div>
    <div class="popup_all__descript">
        <p>Мы свяжемся с Вами в ближайшее время</p>
    </div>
</div>
