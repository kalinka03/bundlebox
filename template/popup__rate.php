<div id="js_answer__rate" class="popup_all rate__popup">
    <div class="js__answer-wrap">
        <div class="popup_all__title">
            <h2>Стоимость доставки</h2>
        </div>
        <div class="popup_all__line"></div>
        <div class="popup_all__descript">
            <p>Для получения подробной информации заполните форму и наш менеджер свяжется с Вами в ближайшее время</p>
        </div>

        <div id="js__rate" class="popup_all__form">
            <form class="form-all form-group rate__form">
                <div class="center-input">
                    <input type="hidden" name="val" value="Стоимость доставки">
                    <input class="popup-call__name" type="text" name="name" placeholder="Ваше имя">
                    <input class="popup-call__phone maskPhone" type="text" name="phone" placeholder="Номер телефона">
                    <input class="popup-call__email" type="email" name="email" placeholder="Email">
                    <input class="popup-call__city" type="text" name="city" placeholder="Город доставки">
                    <div class="rate__input">
                        <div class="rate__size rate__write">
                            <p>Вес,кг</p>
                            <div class="rate__write-inp">
                                <input class="popup-size" type="number" name="size" placeholder="Вес">
                            </div>
                        </div>
                        <div class="rate__length rate__write">
                            <p>Длина, см</p>
                            <div class="rate__write-inp">
                                <input class="popup-length" type="number" name="length" placeholder="Длина">
                            </div>
                        </div>
                        <div class="rate__weight rate__write">
                            <p>Ширина, см</p>
                            <div class="rate__write-inp">
                                <input class="popup-weigth" type="number" name="beam" placeholder="Ширина">
                            </div>
                        </div>
                        <div class="rate__height rate__write">
                            <p>Высота, см</p>
                            <div class="rate__write-inp">
                                <input class="popup-height" type="number" name="level" placeholder="Высота">
                            </div>
                        </div>
                    </div>
                    <textarea class="popup-picking__text" rows="3" name="commit"
                              placeholder="Опишите что необходимо доставить"></textarea>
                    <div class="btn__form ">
                        <button class="button__orange form__orange rate__btn-fancybox"> Отправить</button>

                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<div id="js__rate-after" class="popup_all rate__popup-after">
        <div class="popup_all__title">
            <h2>Ваша заявка принята</h2>
        </div>
        <div class="popup_all__line"></div>
        <div class="popup_all__descript">
            <p>Мы свяжемся с Вами в ближайшее время</p>
        </div>
    </div>



