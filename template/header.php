<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
<title>Bundle Box</title>
<meta name="description" content="Description of the page less than 150 characters">
<link rel="shortcut icon" href="/img/favicon.png" type="image/png">
<link rel="stylesheet" type="text/css" href="/fonts/fonts.css">
<link rel="stylesheet" type="text/css" href="/plugin/bootstrap-4.1.2/bootstrap-4.1.2/dist/css/bootstrap.min.css">

<!-- animate  -->
<link rel="stylesheet" type="text/css" href="/plugin/animate/animate.css">
<!-- animate  -->

<link rel="stylesheet" type="text/less" href="/css/planeta.less">
<link rel="stylesheet" type="text/less" href="/css/template.less">
<link rel="stylesheet" type="text/less" href="/css/footer.less">
<link rel="stylesheet" type="text/less" href="/css/button.less">
<link rel="stylesheet" type="text/css" href="/css/hamburger.css">
<link rel="stylesheet" type="text/less" href="/css/main.less">
<link rel="stylesheet" type="text/less" href="/css/discont.less">
<link rel="stylesheet" type="text/css" href="/css/discount/discountMedia.css">
<link rel="stylesheet" type="text/css" href="/css/media.css">

<!-- OwlCarousel  -->
<link rel="stylesheet" type="text/css" href="/plugin/OwlCarousel2-2.3.4/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="/plugin/OwlCarousel2-2.3.4/owl.theme.default.min.css">
<!-- OwlCarousel  -->

<!-- FormStyler  -->
<link href="/plugin/FormStyler/jquery.formstyler.css" rel="stylesheet" />
<link href="/plugin/FormStyler/jquery.formstyler.theme.css" rel="stylesheet" />
<!-- FormStyler -->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<!-- slick-->
<link rel="stylesheet" type="text/css" href="/plugin/slick-1.8.1/slick.css">
<!-- slick -->

<link rel="stylesheet" type="text/css"  href="/plugin/slick-1.8.1/slick.css">

<!--<link rel="stylesheet" href="/plugin/fontawesome-free-5.1.0-web/css/all.css">-->

<!-- aos-->
<link rel="stylesheet" type="text/css" href="/plugin/aos-master/aos.css">
<!-- aos  -->

<!-- fancybox -->
<link rel="stylesheet" type="text/css" href="/plugin/fancybox-master/jquery.fancybox.css">
<!--fancybox-->




<script type="text/javascript" src="/plugin/Jquery-3.1/jquery.min.js"></script>

<!-- parallax -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
<!-- parallax -->

<!-- FormStyler  -->
<script type="text/javascript" src="/plugin/FormStyler/jquery.formstyler.min.js"></script>
<!-- FormStyler -->


<!--<!-- woow-->
<script type="text/javascript" src="/plugin/WOW-1.1.0/wow.min.js"></script>
<script type="text/javascript" src="/plugin/WOW-1.1.0/wow.min.js"></script>
<!--<!-- woow-->

<!-- bootstrap -->
<script type="text/javascript" src="/plugin/bootstrap-4.1.2/bootstrap-4.1.2/dist/js/bootstrap.min.js"></script>
<!-- bootstrap -->
<!-- OwlCarousel  -->
<script type="text/javascript" src="/plugin/OwlCarousel2-2.3.4/owl.carousel.min.js"></script>
<!-- OwlCarousel  -->
<!--<script type="text/javascript" src="http://merritt-wellness.com/owl-carousel/owl.carousel.js"></script>-->

<!-- fancybox-->
<script type="text/javascript" src="/plugin/fancybox-master/jquery.fancybox.js"></script>
<!-- fancybox-->


<!-- less-->
<script type="text/javascript" src="/plugin/less/dist/less.min.js"></script>
<!-- less-->


<!--<!-- timer -->
<!--    <script type="text/javascript" src="/plugin/timer/timer.min.js"></script>-->
<!--<!-- timer -->



<!-- aos-->
<script type="text/javascript" src="/plugin/aos-master/aos.js"></script>
<!-- aos  -->

<!-- slick-->
<script type="text/javascript" src="/plugin/slick-1.8.1/slick.js"></script>
<!-- slick -->


<!-- mask-->
<script type="text/javascript" src="/plugin/maskPhone/jquery.maskedinput.min.js"></script>
<!-- mask-->

<!-- masonry-->
<script type="text/javascript" src="/plugin/Masonry/masonry.js"></script>
    <script type="text/javascript" src="/plugin/Masonry/masonry-docs.min.js"></script>
<!-- masonry  -->




<!-- validate-->
<script type="text/javascript" src="/plugin/jquery-validation-1.17.0/jquery.validate.js"></script>
<!-- validate-->





<script type="text/javascript" src="/js/allScript/all.js"></script>
<script type="text/javascript" src="/js/hamburger.js"></script>
<script type="text/javascript" src="/js/header/header.js"></script>
<script type='text/javascript' src="/plugin/aSendForm/assets/jquery.send.form.js"></script>


<script type='text/javascript' src="/plugin/aSendForm/assets/additional-methods.min.js"></script>


<!--<script type='text/javascript' src="/plugin/aSendForm/assets/jquery.validate.min.js"></script>-->
<!--<script type='text/javascript' src="/plugin/aSendForm/assets/additional-methods.min.js"></script>-->
<script type='text/javascript' src="/js/formSend.js"></script>



</head>
<body>
