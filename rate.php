<?php include("template/header-main.php") ?>
<link rel="stylesheet" href="plugin/fontawesome-free-5.1.0-web/css/all.css">

<!--<link rel="stylesheet" href="css/rate/rate.css">-->
<link rel="stylesheet" href="css/rate/rate.css">
<link rel="stylesheet" href="css/rate/rate__media.css">

<div class="rate">
    <div class="rate__top">
        <div class=" rate__wrapper container">
            <div class="rate__content ">
                <div class="rate__title ">
                    <h2>Тарифы</h2>
                    <div class="rate__img">
                        <img src="img/rate/rate__title.png" alt="title__rate">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="rate__info container">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="rate__info-text">
                    <p>
                        Наши тарифы предельно просты – Вы платите только за доставку вещи. Стоимость доставки вещей
                        легко найти в таблицах стоимости доставки.
                    </p>
                    <p>
                        Как рассчитать стоимость доставки заказа. К примеру, если Вы заказали одну футболку, стоимость
                        ее доставки – 100 рублей. Если в заказе три футболки, то стоимость доставки будет 100 рублей * 3
                        футболки, то есть 300 рублей.
                    </p>
                    <p>
                        Если Вам нужна помощь или Вы не нашли стоимость доставки для интересующей Вас вещи, обращайтесь,
                        мы с радостью поможем и все разъясним.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php if (isset($_GET['login'])): ?>
        <div class="rate__global">
            <div class=" container">
                <div class="rate__global-title">
                    <h2>
                        Калькулятор доставки
                    </h2>
                </div>
                <div class="global__content-wrapper active">
                    <div class="row global__content-row justify-content-center">
                        <div class="col-md-11">
                            <div class="global__tabs">
                                <div class="global__tabs-title">
                                    <p>
                                        Откуда:
                                    </p>
                                    <ul class="global__caption">
                                        <li class="active">
                                <span>
                                    США
                                </span>
                                        </li>
                                        <li>
                                <span>
                                    Англия
                                </span>
                                        </li>
                                        <li>
                                <span>
                                    Германия
                                </span>
                                        </li>
                                        <li>
                                <span>
                                    Италия
                                </span>
                                        </li>
                                    </ul>
                                </div>

                                <div class="global__content active">
                                    <div class="global__content-form">
                                        <form class="global__form">
                                            <div class="global__where global__input">
                                                <div class="row align-items-center">
                                                    <p class=" global__form-name col-md-3">
                                                        Куда:
                                                    </p>
                                                    <input class="col-md-6 col-10" type="text" name="where" placeholder="Город или регион РФ">
                                                </div>
                                            </div>
                                            <div class="global__size global__input">
                                                <div class="row align-items-center">
                                                    <p  class="global__form-name col-md-3">
                                                        Вес посылки, кг:
                                                    </p>
                                                    <input class="col-md-2 col-4 " type="number" name="where" placeholder="Вес">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="global__content-info table__rate">
                                        <div class="row global__content-info-wrapp">
                                            <div class="table__name col-sm-3 col-6">
                                                <ul class="table__name-list">
                                                    <li class="table__delivery table__tr">
                                                        Способ доставки:
                                                    </li>
                                                    <li class="table__cost table__tr">
                                                        Стоимость доставки:
                                                    </li>
                                                    <li class="table__time table__tr">
                                                        Срок доставки:
                                                    </li>
                                                    <li class="table__insurance table__tr">
                                                        Страховка:
                                                    </li>
                                                    <li class="table__courier table__tr">
                                                        Курьер:
                                                    </li>
                                                    <li class="table__dimensions table__tr">
                                                        Максимальные габариты:
                                                    </li>
                                                    <li class="table__weight table__tr">
                                                        Максимальный вес:
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="table__sliders col-sm-9 col-6">
                                                <div class="row table__sliders-js owl-carousel">
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/boxberry.png" alt="delivery">
                                                            </div>
                                                            <div class="table__delivery-text table__text">
                                                                <p>
                                                                    Boxberry
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text table__text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/carrgo.png" alt="carrgo">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    Cargo
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                80 GBP / 6892.45 RUB
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/post__russia.png" alt="post">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    Почта России
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                30 EUR / 2257.24 RUB
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/ems.png" alt="ems">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    EMS
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                100 EUR / 2257.24 RUB
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/boxberry.png" alt="delivery">
                                                            </div>
                                                            <div class="table__delivery-text table__text">
                                                                <p>
                                                                    Boxberry
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text table__text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="global__content ">
                                    <div class="global__content-form">
                                        <form class="global__form">
                                            <div class="global__where global__input">
                                                <div class="row align-items-center">
                                                    <p class=" global__form-name col-md-3">
                                                        Куда:
                                                    </p>
                                                    <input class="col-md-6 col-10" type="text" name="where" placeholder="Город или регион РФ">
                                                </div>
                                            </div>
                                            <div class="global__size global__input">
                                                <div class="row align-items-center">
                                                    <p  class="global__form-name col-md-3">
                                                        Вес посылки, кг:
                                                    </p>
                                                    <input class="col-md-2 col-4 " type="number" name="where" placeholder="Вес">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="global__content-info table__rate">
                                        <div class="row global__content-info-wrapp">
                                            <div class="table__name col-sm-3 col-6">
                                                <ul class="table__name-list">
                                                    <li class="table__delivery table__tr">
                                                        Способ доставки:
                                                    </li>
                                                    <li class="table__cost table__tr">
                                                        Стоимость доставки:
                                                    </li>
                                                    <li class="table__time table__tr">
                                                        Срок доставки:
                                                    </li>
                                                    <li class="table__insurance table__tr">
                                                        Страховка:
                                                    </li>
                                                    <li class="table__courier table__tr">
                                                        Курьер:
                                                    </li>
                                                    <li class="table__dimensions table__tr">
                                                        Максимальные габариты:
                                                    </li>
                                                    <li class="table__weight table__tr">
                                                        Максимальный вес:
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="table__sliders col-sm-9 col-6">
                                                <div class="row table__sliders-js owl-carousel">
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/boxberry.png" alt="delivery">
                                                            </div>
                                                            <div class="table__delivery-text table__text">
                                                                <p>
                                                                    Boxberry
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text table__text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/carrgo.png" alt="carrgo">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    Cargo
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/post__russia.png" alt="post">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    Почта России
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/ems.png" alt="ems">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    EMS
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/boxberry.png" alt="delivery">
                                                            </div>
                                                            <div class="table__delivery-text table__text">
                                                                <p>
                                                                    Boxberry
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text table__text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="global__content ">
                                    <div class="global__content-form">
                                        <form class="global__form">
                                            <div class="global__where global__input">
                                                <div class="row align-items-center">
                                                    <p class=" global__form-name col-md-3">
                                                        Куда:
                                                    </p>
                                                    <input class="col-md-6 col-10" type="text" name="where" placeholder="Город или регион РФ">
                                                </div>
                                            </div>
                                            <div class="global__size global__input">
                                                <div class="row align-items-center">
                                                    <p  class="global__form-name col-md-3">
                                                        Вес посылки, кг:
                                                    </p>
                                                    <input class="col-md-2 col-4 " type="number" name="where" placeholder="Вес">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="global__content-info table__rate">
                                        <div class="row global__content-info-wrapp">
                                            <div class="table__name col-sm-3 col-6">
                                                <ul class="table__name-list">
                                                    <li class="table__delivery table__tr">
                                                        Способ доставки:
                                                    </li>
                                                    <li class="table__cost table__tr">
                                                        Стоимость доставки:
                                                    </li>
                                                    <li class="table__time table__tr">
                                                        Срок доставки:
                                                    </li>
                                                    <li class="table__insurance table__tr">
                                                        Страховка:
                                                    </li>
                                                    <li class="table__courier table__tr">
                                                        Курьер:
                                                    </li>
                                                    <li class="table__dimensions table__tr">
                                                        Максимальные габариты:
                                                    </li>
                                                    <li class="table__weight table__tr">
                                                        Максимальный вес:
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="table__sliders col-sm-9 col-6">
                                                <div class="row table__sliders-js owl-carousel">
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/boxberry.png" alt="delivery">
                                                            </div>
                                                            <div class="table__delivery-text table__text">
                                                                <p>
                                                                    Boxberry
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text table__text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/carrgo.png" alt="carrgo">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    Cargo
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/post__russia.png" alt="post">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    Почта России
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/ems.png" alt="ems">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    EMS
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/boxberry.png" alt="delivery">
                                                            </div>
                                                            <div class="table__delivery-text table__text">
                                                                <p>
                                                                    Boxberry
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text table__text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="global__content ">
                                    <div class="global__content-form">
                                        <form class="global__form">
                                            <div class="global__where global__input">
                                                <div class="row align-items-center">
                                                    <p class=" global__form-name col-md-3">
                                                        Куда:
                                                    </p>
                                                    <input class="col-md-6 col-10" type="text" name="where" placeholder="Город или регион РФ">
                                                </div>
                                            </div>
                                            <div class="global__size global__input">
                                                <div class="row align-items-center">
                                                    <p  class="global__form-name col-md-3">
                                                        Вес посылки, кг:
                                                    </p>
                                                    <input class="col-md-2 col-4 " type="number" name="where" placeholder="Вес">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="global__content-info table__rate">
                                        <div class="row global__content-info-wrapp">
                                            <div class="table__name col-sm-3 col-6">
                                                <ul class="table__name-list">
                                                    <li class="table__delivery table__tr">
                                                        Способ доставки:
                                                    </li>
                                                    <li class="table__cost table__tr">
                                                        Стоимость доставки:
                                                    </li>
                                                    <li class="table__time table__tr">
                                                        Срок доставки:
                                                    </li>
                                                    <li class="table__insurance table__tr">
                                                        Страховка:
                                                    </li>
                                                    <li class="table__courier table__tr">
                                                        Курьер:
                                                    </li>
                                                    <li class="table__dimensions table__tr">
                                                        Максимальные габариты:
                                                    </li>
                                                    <li class="table__weight table__tr">
                                                        Максимальный вес:
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="table__sliders col-sm-9 col-6">
                                                <div class="row table__sliders-js owl-carousel">
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/boxberry.png" alt="delivery">
                                                            </div>
                                                            <div class="table__delivery-text table__text">
                                                                <p>
                                                                    Boxberry
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text table__text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/carrgo.png" alt="carrgo">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    Cargo
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/post__russia.png" alt="post">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    Почта России
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/ems.png" alt="ems">
                                                            </div>
                                                            <div class="table__delivery-text">
                                                                <p>
                                                                    EMS
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="table__slider col-md-3">
                                                        <div class="table__tr table__delivery">
                                                            <div class="table__delivery-icon">
                                                                <img src="img/rate/boxberry.png" alt="delivery">
                                                            </div>
                                                            <div class="table__delivery-text table__text">
                                                                <p>
                                                                    Boxberry
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__cost">
                                                            <div class="table__cost-text table__text">
                                                            <span>
                                                                неизвестно
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__time">
                                                            <div class="table__time-text">
                                                            <span>
                                                                10-20 дней
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__insurance">
                                                            <div class="table__insurance-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__courier">
                                                            <div class="table__courier-text">
                                                            <span>
                                                                +
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__dimensions">
                                                            <div class="table__dimensions">
                                                            <span>
                                                                Д+Ш+В < 300 см
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="table__tr table__weight">
                                                            <div class="table__weight">
                                                            <span>
                                                                31.5 кг
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <?php else: ?>
        <div class="rate__irkutsk ">
            <div class="irkutsk__title container">
                <h2>
                    Таблица стоимости доставки
                </h2>
            </div>
            <div class="irkutsk__tabs ">
                    <div class="irkutst__tabs-title container">
                        <p>
                            Стимость доставки для:
                        </p>
                        <ul class="irkutsk__caption">
                            <li class="active irkutsk__active-before">
                            <span>
                                США
                            </span>
                            </li>
                            <li>
                            <span>
                                ЕВРОСОЮЗ
                            </span>
                            </li>
                        </ul>
                    </div>
                    <div class="irkutsk__content-bcg">
                        <div class="irkutsk__content container active">
                            <div class="row">
                                <div class="col-xl-4 col-md-6">
                                    <div class="female__things">
                                        <div class="things__title">
                                            <h2>
                                                Женские вещи
                                            </h2>
                                            <span class="things__plus">
                                    <i class="fas fa-minus"></i>
                                </span>
                                        </div>
                                        <div class="things__content">
                                            <div class="things__content-title">
                                    <span class="things__content-name">
                                        Наименование
                                    </span>
                                                <span class="things__content-price">
                                        Цена, RUB
                                    </span>
                                            </div>
                                            <ul class="things__goods">
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           120
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Кофта с рукавом
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           345
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Ветровка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                            100
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           235
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           300
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Кофта с рукавом
                                       </span>
                                                    <span class="goods__price">
                                           745
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           250
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер. толстовка
                                       </span>
                                                    <span class="goods__price">
                                           125
                                       </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6">
                                    <div class="female__things">
                                        <div class="things__title">
                                            <h2>
                                                Мужские вещи
                                            </h2>
                                            <span class="things__plus">
                                    <i class="fas fa-minus"></i>
                                </span>
                                        </div>
                                        <div class="things__content">
                                            <div class="things__content-title">
                                    <span class="things__content-name">
                                        Наименование
                                    </span>
                                                <span class="things__content-price">
                                        Цена, RUB
                                    </span>
                                            </div>
                                            <ul class="things__goods">
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           120
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Кофта с рукавом
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           345
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Ветровка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                            100
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           235
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           300
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6">
                                    <div class="female__things">
                                        <div class="things__title">
                                            <h2>
                                                Детские вещи
                                            </h2>
                                            <span class="things__plus">
                                    <i class="fas fa-minus"></i>
                                </span>
                                        </div>
                                        <div class="things__content">
                                            <div class="things__content-title">
                                    <span class="things__content-name">
                                        Наименование
                                    </span>
                                                <span class="things__content-size">
                                        Размер
                                    </span>
                                                <span class="things__content-price">
                                        Цена, RUB
                                    </span>
                                            </div>
                                            <ul class="things__goods-child">
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Футболка
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info ">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="child__info-img ">
                                                    <span>
                                                        Футболка
                                                    </span>
                                                                </div>
                                                            </div>
                                                            <ul class="child__info-list col-6">
                                                                <li class="info__list-item">
                                                    <span class="child__info-size">
                                                        62-92 см
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                    <span class="child__info-size">
                                                        92-128 см
                                                    </span>
                                                                    <span class="child__info-price">
                                                       345
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                    <span class="child__info-size">
                                                        128-152 см
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                    <span class="child__info-size">
                                                        125-182 см
                                                    </span>
                                                                    <span class="child__info-price">
                                                       100
                                                   </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Кофта с рукавом
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Кофта с рукавом
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Футболка
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Кофта с рукавом
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Кофта с рукавом
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="irkutsk__content container">
                            <div class="row">
                                <div class="col-xl-4 col-md-6">
                                    <div class="female__things">
                                        <div class="things__title">
                                            <h2>
                                                Мужские вещи
                                            </h2>
                                            <span class="things__plus">
                                    <i class="fas fa-minus"></i>
                                </span>
                                        </div>
                                        <div class="things__content">
                                            <div class="things__content-title">
                                    <span class="things__content-name">
                                        Наименование
                                    </span>
                                                <span class="things__content-price">
                                        Цена, RUB
                                    </span>
                                            </div>
                                            <ul class="things__goods">
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           120
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Кофта с рукавом
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           345
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Ветровка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                            100
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           235
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           300
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Кофта с рукавом
                                       </span>
                                                    <span class="goods__price">
                                           745
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           250
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер. толстовка
                                       </span>
                                                    <span class="goods__price">
                                           125
                                       </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6">
                                    <div class="female__things">
                                        <div class="things__title">
                                            <h2>
                                                Женские вещи
                                            </h2>
                                            <span class="things__plus">
                                    <i class="fas fa-minus"></i>
                                </span>
                                        </div>
                                        <div class="things__content">
                                            <div class="things__content-title">
                                    <span class="things__content-name">
                                        Наименование
                                    </span>
                                                <span class="things__content-price">
                                        Цена, RUB
                                    </span>
                                            </div>
                                            <ul class="things__goods">
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           120
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Кофта с рукавом
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           345
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Ветровка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                            100
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           235
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Куртка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           300
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Свитер, толстовка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                                <li class="goods__item">
                                       <span class="goods__name">
                                           Футболка
                                       </span>
                                                    <span class="goods__price">
                                           450
                                       </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6">
                                    <div class="female__things">
                                        <div class="things__title">
                                            <h2>
                                                Детские вещи
                                            </h2>
                                            <span class="things__plus">
                                    <i class="fas fa-minus"></i>
                                </span>
                                        </div>
                                        <div class="things__content">
                                            <div class="things__content-title">
                                    <span class="things__content-name">
                                        Наименование
                                    </span>
                                                <span class="things__content-size">
                                        Размер
                                    </span>
                                                <span class="things__content-price">
                                        Цена, RUB
                                    </span>
                                            </div>
                                            <ul class="things__goods-child">
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Футболка
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info ">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="child__info-img ">
                                                    <span>
                                                        Футболка
                                                    </span>
                                                                </div>
                                                            </div>
                                                            <ul class="child__info-list col-6">
                                                                <li class="info__list-item">
                                                    <span class="child__info-size">
                                                        62-92 см
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                    <span class="child__info-size">
                                                        92-128 см
                                                    </span>
                                                                    <span class="child__info-price">
                                                       345
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                    <span class="child__info-size">
                                                        128-152 см
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                    <span class="child__info-size">
                                                        125-182 см
                                                    </span>
                                                                    <span class="child__info-price">
                                                       100
                                                   </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Кофта с рукавом
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Кофта с рукавом
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Футболка
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Кофта с рукавом
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="goods__child">
                                                    <div class="accord goods__child-title">
                                            <span class="goods__child-name">
                                                Кофта с рукавом
                                            </span>
                                                        <span class="goods__child-arrow">
                                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px"
                                                     viewBox="0 0 15.6 15.5"
                                                     style="enable-background:new 0 0 15.6 15.5;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #434343;
                                                    }
                                                </style>
                                                <g>
                                                    <g>
                                                        <path class="st0" d="M15.4,12.3c-0.1,0.1-0.3,0.2-0.4,0.2c-0.3,0-0.6-0.3-0.6-0.6l0-9.8L1,15.4c-0.2,0.2-0.6,0.2-0.8,0
                                                            c-0.2-0.2-0.2-0.6,0-0.8L13.5,1.2l-9.8,0C3.6,1.2,3.4,1.1,3.3,1C3.2,0.9,3.1,0.8,3.1,0.6c0-0.2,0.1-0.3,0.2-0.4
                                                            C3.4,0.1,3.6,0,3.7,0L15,0c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.2,0.3,0.2,0.4l0,11.3C15.6,12,15.5,12.2,15.4,12.3z"/>
                                                    </g>
                                                </g>
                                                </svg>
                                            </span>
                                                    </div>
                                                    <div class="panel goods__child-info row">
                                                        <div class="col-12">
                                                            <ul class="child__info-list">
                                                                <li class="info__list-item">
                                                    <span class="child__info-name">
                                                        Футболка
                                                    </span>
                                                                    <span class="child__info-size">
                                                        -
                                                    </span>
                                                                    <span class="child__info-price">
                                                       450
                                                   </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                                <li class="info__list-item">
                                                        <span class="child__info-name">
                                                            Шапка
                                                        </span>
                                                                    <span class="child__info-size">
                                                            -
                                                        </span>
                                                                    <span class="child__info-price">
                                                            345
                                                        </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

        </div>
    <?php endif; ?>

</div>
<div class="rate__question">
    <section class="set-question">
        <div class="set-question__line module wow animated" data-wow-offset="200"
             style="visibility: visible; animation-name: increase;">
        </div>
        <!--    <canvas id="paper"></canvas>-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="set-question__wrapper wow fadeInRight animated" data-wow-offset="400"
                         data-wow-delay="0.4s" data-wow-duration="2s"
                         style="visibility: visible; animation-duration: 2s; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <h3>Если вы не нашли стоимости доставки</h3>
                        <h4>для интересующей вас категории товара или хотите оговорить индивидуальные условия
                            доставки </h4>
                        <div class="set-question__popup">
                            <a href="#js_answer__rate" class="button__white modal-trigger rate__button">Запросить
                                стоимость индивидуально</a>
                        </div>
                        <div class="set-question__girl">
                            <img src="img/main/block8/girl.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include("template/footer.php") ?>
<?php include("template/popup__rate.php") ?>
<?php include("template/popupMain.php") ?>

<script type='text/javascript' src="plugin/aSendForm/assets/jquery.send.form.js"></script>

<script type='text/javascript' src="js/formSend.js"></script>
<script type="text/javascript" src="/js/fancyboxMain.js"></script>
<script src="js/rate/rate.js"></script>

</body>
</html>